<?php

namespace Techies\CustomPrice\Model\Total\Quote;

class AdditionalTotal extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return  $this|bool
     */
    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        parent::collect($quote, $shippingAssignment, $total);

        $address = $shippingAssignment->getShipping()->getAddress();
        $address->setTotalQty(0);
        /**
         * Process address items
         */
        $items = $shippingAssignment->getItems();
        $addtionalAmountTotal = 0;
        foreach ($items as $item) {
            if ($item->getQty() > 3) {
                /**
                 * Separately calculate subtotal of Addtional Charges
                 */
                $addtionalAmountTotal += $item->getTotalInclExtraPrice();
                $item->setRowTotal(0);
            }
        }
        $address->setSubtotal($total->getSubtotal() + $addtionalAmountTotal);
        $address->setBaseSubtotal($total->getBaseSubtotal() + $addtionalAmountTotal);
        $total->setGrandTotal($total->getGrandTotal() + $addtionalAmountTotal);
        $total->setBaseGrandTotal($total->getBaseGrandTotal() + $addtionalAmountTotal);
        return $this;
    }
}
