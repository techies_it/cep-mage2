<?php

namespace Techies\CustomPrice\Plugin\Model;

class AdditionalTotalAddInSubtotal
{

    protected $additionalRowTotalPrice = null;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
    ) {
        $this->priceCurrency = $priceCurrency;
    }

    public function afterCalcRowTotal(\Magento\Quote\Model\Quote\Item $subject, $result)
    {

        if ($result->getTotalInclExtraPrice() !== null) {
            $roundRowTotal=$result->getRowTotal() + $result->getTotalInclExtraPrice();
            $roundBaseRowTotal=$result->getBaseRowTotal() + $result->getTotalInclExtraPrice();
            $result->setRowTotal($this->priceCurrency->round($roundRowTotal));
            $result->setBaseRowTotal($this->priceCurrency->round($roundBaseRowTotal));
        }
        return $result;
    }
}
