<?php

namespace Techies\CustomPrice\Observer;

use Magento\Framework\View\Result\PageFactory;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\RequestInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;

class CustomPrice implements \Magento\Framework\Event\ObserverInterface
{
    /*
    * Sample Max Qty Path
    */
    const SAMPLE_MAX_QTY_PATH = 'techies_flatrate/flatrate/sample_bulk_max_qty';

    public function __construct(
        PageFactory $resultPageFactory,
        Product $product,
        RequestInterface $request,
        \Magento\Framework\View\Element\Template\Context $context,
        CollectionFactory $productCollectionFactory,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->scopeConfig = $scopeConfig;
        $this->product = $product;
        $this->_request = $request;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->storeManager = $context->getStoreManager();
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $param = $this->_request->getParams();
        $currentProId = $param['product'];
        $item = $observer->getEvent()->getQuoteItem();

        $_product = $this->product->load($currentProId);
        $customPostData = $this->getCustomPostValue($param);
        if ($param['qty'] > $this->sampleProductBulkMaxQty()) {
            /* call custom additional options function */
            $this->setCustomAddtionalOptions($customPostData, $item);
            /* get custom option price behalf of Production Time */
            $production = $customPostData['prod_time']['value'];
            $optionPrice = $this->getCustomOptionPrice($_product, $production);
            /* get min qty based on production time */
            $minQty = $this->getMinimumBulkQty($_product, $production);

            $setupQty = $this->getSetupQtyTotal($_product, $customPostData);
            $less_minimum_fee = 0;
            $item->setLessMinimumFee($less_minimum_fee);
            /* get Custom Price based on Production time's qty */
            $custom_price = $this->getProductionPrice($currentProId, $item->getQty(), $customPostData);
            $item->setCustomPrice($custom_price);
            $item->setOriginalCustomPrice($custom_price);

            $item->setColorImprintFirst($customPostData['color_first']['value']);
            $item->setColorImprintSecond($customPostData['color_first']['value']);
            $item->setColorImprintThird($customPostData['color_first']['value']);

            $item->setBackLogoImprintColorFirst($customPostData['back_color_first']['value']);
            $item->setBackLogoImprintColorSecond($customPostData['back_color_second']['value']);
            $item->setBackLogoImprintColorThird($customPostData['back_color_third']['value']);

            $item->setSideOneLogoImprintColorFirst($customPostData['side1_color_first']['value']);
            $item->setSideOneLogoImprintColorSecond($customPostData['side2_color_second']['value']);
            $item->setSideOneLogoImprintColorThird($customPostData['side2_color_third']['value']);

            $item->setSideTwoLogoImprintColorFirst($customPostData['side2_color_first']['value']);
            $item->setSideTwoLogoImprintColorSecond($customPostData['side2_color_second']['value']);
            $item->setSideTwoLogoImprintColorThird($customPostData['side2_color_third']['value']);

            $item->setPrintSides($customPostData['print_sides']['value']);
            $item->setArtworkType($customPostData['artwork_logos']['value']);
            $item->setLogoColors($customPostData['front_color']['value']);
            $item->setBackLogo($customPostData['back_color']['value']);
            $item->setSideOneLogo($customPostData['side1_color']['value']);
            $item->setSideTwoLogo($customPostData['side2_color']['value']);
            $item->setProductionTime($customPostData['prod_time']['value']);

            $item->setFullColorPrice($optionPrice['full_color_price']);
            $item->setSetupPrice($optionPrice['setup_price']);
            $paramsArray = ['product' => $_product,
                'color_price' => $optionPrice['color_price'],
                'item' => $item,
                'customPostData' => $customPostData
            ];
            $item->setColorPrice($this->getCustomColorPrice($paramsArray));
            $item->setSetupQty($setupQty);

            $totalFullColorPrice = $item->getQty() * $item->getFullColorPrice();
            $totalColorPrice = $item->getQty() * $item->getColorPrice();
            if ($item->getSetupQty() > 0) {
                $totalSetupPrice = $item->getSetupQty() * $item->getSetupPrice();
                $item->setSetupFeesTotal($totalSetupPrice);
            }

            $this->getAdditionalImprintPriceTotal($item, $customPostData);
            $this->getFullImprintPriceTotal($item, $_product, $customPostData);

            $addtionalCharges = 0;
            $addtionalCharges += $item->getSecondImprintColorTotal();
            $addtionalCharges += $item->getThirdImprintColorTotal();
            $addtionalCharges += $item->getFullImprintColorTotal();
            $addtionalCharges += $item->getFullImprintColorFront();
            $addtionalCharges += $item->getFullImprintColorBack();
            $addtionalCharges += $item->getFullImprintColorFirstSide();
            $addtionalCharges += $item->getFullImprintColorSecondSide();
            $addtionalCharges += $item->getFirstSideSecondImprintColorTotal();
            $addtionalCharges += $item->getFirstSideThirdImprintColorTotal();
            $addtionalCharges += $item->getFirstSideFourthImprintColorTotal();
            $addtionalCharges += $item->getSecondSideFirstImprintColorTotal();
            $addtionalCharges += $item->getSecondSideSecondImprintColorTotal();
            $addtionalCharges += $item->getSecondSideThirdImprintColorTotal();
            $addtionalCharges += $item->getSecondSideFourthImprintColorTotal();
            $addtionalCharges += $item->getThirdSideFirstImprintColorTotal();
            $addtionalCharges += $item->getThirdSideSecondImprintColorTotal();
            $addtionalCharges += $item->getThirdSideThirdImprintColorTotal();
            $addtionalCharges += $item->getFourthSideFirstImprintColorTotal();
            $addtionalCharges += $item->getFourthSideSecondImprintColorTotal();
            $addtionalCharges += $item->getFourthSideThirdImprintColorTotal();

            $addtionalCharges += $item->getSetupFeesTotal();
            $addtionalCharges += $item->getLessMinimumFee();

            $item->setTotalInclExtraPrice($addtionalCharges);

        }
        $item->getProduct()->setIsSuperMode(true);
    }

    /*
     * Save Custom Options value in addtional options
     */
    private function setCustomAddtionalOptions($post, $item)
    {
        $additionalOptions = [];
        if ($additionalOption = $item->getOptionByCode('additional_options')) {
            $additionalOptions = (array)json_decode($additionalOption->getValue());
        }

        if (is_array($post)) {
            foreach ($post as $key => $val) {
                if ($key == '' || $val == '') {
                    continue;
                }

                if (!empty($val['value'])) {
                    if ($key == 'prod_time') {
                        $value = $this->getProductionTimeLabel($val['value']);
                    } elseif ($key == 'print_sides') {
                        $value = $this->getPrintedSidesLabel($val['value']);
                    } elseif ($key == 'artwork_logos') {
                        $value = $this->getArtworkLabel($val['value']);
                    } else {
                        $value = $val['value'];
                    }
                    $additionalOptions[] = [
                        'label' => $val['label'],
                        'value' => $value
                    ];
                }
            }
        }
        if (count($additionalOptions) > 0) {
            $item->addOption([
                'product_id' => $item->getProductId(),
                'code' => 'additional_options',
                'value' => json_encode($additionalOptions)
            ]);
        }
    }

    /*
    *  Set Production Time label based on it's value.
    */
    private function getProductionTimeLabel($value)
    {
        if ($value == 1) {
            $production_label = '2 Weeks or Less';
        }
        if ($value == 2) {
            $production_label = 'Within 3 Weeks';
        }
        if ($value == 3) {
            $production_label = 'Within 30 - 45 Days';
        }
        if ($value == 4) {
            $production_label = 'Within 60 - 90 Days';
        }
        return $production_label;
    }

    /*
    *  Set Printed Sides label based on it's value.
    */
    private function getPrintedSidesLabel($value)
    {
        if ($value == 1) {
            $label = 'Front Only';
        }
        if ($value == 2) {
            $label = 'Front & Back';
        }
        if ($value == 3) {
            $label = 'Front, Back & Sides';
        }
        return $label;
    }

    /*
    *  Set Artwork Type label based on it's value.
    */
    private function getArtworkLabel($value)
    {
        if (!empty($value)) {
            $label = 'Different Artwork';
        } else {
            $label = 'Same Artwork';
        }
        return $label;
    }

    /*
    *  Get Custom Options post value.
    */
    private function getCustomPostValue($param)
    {
        if (is_array($param)) {
            $prod_time = !empty($param['prod_time']) ? $param['prod_time'] : '';
            $print_sides = !empty($param['print_sides']) ? $param['print_sides'] : '';
            $artwork = !empty($param['artwork_logos']) ? $param['artwork_logos'] : '';
            $logo_colors = !empty($param['front_color']) ? $param['front_color'] : '';
            $back_logo_color = !empty($param['back_color']) ? $param['back_color'] : '';
            $side1_logo_color = !empty($param['side1_color']) ? $param['side1_color'] : '';
            $side2_logo_color = !empty($param['side2_color']) ? $param['side2_color'] : '';
            $imprint_color_1 = !empty($param['color_first']) ? $param['color_first'] : '';
            $imprint_color_2 = !empty($param['color_second']) ? $param['color_second'] : '';
            $imprint_color_3 = !empty($param['color_third']) ? $param['color_third'] : '';

            $back_imprint_color_1 = !empty($param['back_color_first']) ? $param['back_color_first'] : '';
            $back_imprint_color_2 = !empty($param['back_color_second']) ? $param['back_color_second'] : '';
            $back_imprint_color_3 = !empty($param['back_color_third']) ? $param['back_color_third'] : '';

            $side1_imprint_color_1 = !empty($param['side1_color_first']) ? $param['side1_color_first'] : '';
            $side1_imprint_color_2 = !empty($param['side1_color_second']) ? $param['side1_color_second'] : '';
            $side1_imprint_color_3 = !empty($param['side1_color_third']) ? $param['side1_color_third'] : '';

            $side2_imprint_color_1 = !empty($param['side2_color_first']) ? $param['side2_color_first'] : '';
            $side2_imprint_color_2 = !empty($param['side2_color_second']) ? $param['side2_color_second'] : '';
            $side2_imprint_color_3 = !empty($param['side2_color_third']) ? $param['side2_color_third'] : '';

            $customPost = [
                "prod_time" => ['label' => 'Production Time', 'value' => $prod_time],
                "storage_size" => ['label' => 'Storage Size', 'value' => ''],
                "print_sides" => ['label' => 'Print Sides', 'value' => $print_sides],
                "artwork_logos" => ['label' => 'Artwork Type', 'value' => $artwork],
                "front_color" => ['label' => 'Front Logo Color(s)', 'value' => $logo_colors],
                "back_color" => ['label' => 'Back Logo Color(s)', 'value' => $back_logo_color],
                "side1_color" => ['label' => 'Side1 Logo Color(s)', 'value' => $side1_logo_color],
                "side2_color" => ['label' => 'Side2 Logo Color(s)', 'value' => $side2_logo_color],
                "color_first" => ['label' => 'Front Imprint Color - 1', 'value' => $imprint_color_1],
                "color_second" => ['label' => 'Front Imprint Color - 2', 'value' => $imprint_color_2],
                "color_third" => ['label' => 'Front Imprint Color - 3', 'value' => $imprint_color_3],
                "back_color_first" => ['label' => 'Back Imprint Color - 1', 'value' => $imprint_color_1],
                "back_color_second" => ['label' => 'Back Imprint Color - 2', 'value' => $imprint_color_2],
                "back_color_third" => ['label' => 'Back Imprint Color - 3', 'value' => $back_imprint_color_3],
                "side1_color_first" => ['label' => 'Side1 Imprint Color - 1', 'value' => $side1_imprint_color_1],
                "side1_color_second" => ['label' => 'Side1 Imprint Color - 2', 'value' => $side1_imprint_color_2],
                "side1_color_third" => ['label' => 'Side1 Imprint Color - 3', 'value' => $side1_imprint_color_3],
                "side2_color_first" => ['label' => 'Side2 Imprint Color - 1', 'value' => $side2_imprint_color_1],
                "side2_color_second" => ['label' => 'Side2 Imprint Color - 2', 'value' => $side2_imprint_color_2],
                "side2_color_third" => ['label' => 'Side2 Imprint Color - 3', 'value' => $side2_imprint_color_3],
            ];
        }
        return $customPost;
    }

    /*
    * Get Custom option price from backend based on production time.
    * @param $product|$production
    * @return array $option_price
    */
    private function getCustomOptionPrice($product, $production)
    {
        $option_price = [];
        if (in_array($production, [1, 2])) {
            $option_price['setup_price'] = $product->getSet_up_fee();
            $option_price['color_price'] = $product->getAdditional_colors();
            $option_price['full_color_price'] = $product->getFull_color_imprint();
        } else {
            $option_price['setup_price'] = $product->getSetup_fee_overseas();
            $option_price['color_price'] = $product->getAdditional_clolors_overseas();
            $option_price['full_color_price'] = $product->getFull_imprint_overseas();
        }
        return $option_price;
    }

    /*
    *
    * Get Minimum Bulk Qty for Domestic(2 Weeks or 3 Weeks) and Overseas (30-45 days or 60-90 days)
    * @param production_time|$product
    */
    private function getMinimumBulkQty($product, $production)
    {
        $attr_oversease_id = $product->getMinOverseaseProduct();
        $attr_instock_id = $product->getMinInstockProduct();

        if (in_array($production, [3, 4])) {
            $attr = $product->getResource()->getAttribute("min_oversease_product");
            if ($attr->usesSource()) {
                $min_oversease_qty = $attr->getSource()->getOptionText($attr_oversease_id);
                if (is_string($min_oversease_qty)) {
                    $min_oversease_qty = (int)str_replace(",", "", $min_oversease_qty);
                }
                $minQty = $min_oversease_qty;
            }
        } else {
            $attr1 = $product->getResource()->getAttribute("min_instock_product");
            if ($attr1->usesSource()) {
                $min_domestic_qty = $attr1->getSource()->getOptionText($attr_instock_id);
                if (is_string($min_domestic_qty)) {
                    $min_domestic_qty = (int)str_replace(",", "", $min_domestic_qty);
                }
                $minQty = $min_domestic_qty;
            }
        }
        return $minQty;
    }

    /*
    * Set Color Price changed if Imprint Percentage yes or no.
    *
    */
    private function getCustomColorPrice($paramsArray)
    {
        $product = $paramsArray['product'];
        $color_price = $paramsArray['color_price'];
        $item = $paramsArray['item'];
        $customPostData = $paramsArray['customPostData'];
        $imprint_percentage = $product->getImprintPercentage();
        $attr_percentage = $product->getResource()->getAttribute("imprint_percentage");
        if ($attr_percentage->usesSource()) {
            $percentage = $attr_percentage->getSource()->getOptionText($imprint_percentage);
            if ($percentage == 'Yes') {
                $item->setImprintPercentage($color_price);
                $productionPrice = $this->getProductionPrice($product->getId(), $item->getQty(), $customPostData);
                if (isset($productionPrice) && !empty($productionPrice)) {
                    $color_price = $productionPrice * $item->getImprintPercentage();
                    $color_price = preg_replace('/([\d,]+.\d{2})\d/', '$1', $color_price);
                }
            }
        }
        return $color_price;
    }

    /*
     * Get Tier Price based on qty and production time
     * @param $productID
     * @return $tierQtyPice
     */
    public function getProductionPrice($productID, $qty, $customPost)
    {
        $prodtime = $customPost['prod_time']['value'];
        $storageSize = $customPost['storage_size']['value'];
        $productCollection = $this->getProductCollection();
        $productCollection->addAttributeToSelect('e.entity_id')
            ->addAttributeToFilter('entity_id', ['eq' => $productID]);
        $productCollection->getSelect()->joinLeft(
            ['tier' => 'catalog_product_entity_tier_price'],
            'e.entity_id = tier.entity_id',
            ['min(tier.value) as price']
        )
            ->where("tier.production_time = '$prodtime'")
            ->where("tier.qty <=$qty");
        if (!empty($storageSize)) {
            $productCollection->getSelect()->where("tier.customer_group_id = $storageSize");
        }
        $tierprices = $productCollection->getData();
        foreach ($tierprices as $key => $val) {
            $tierQtyPice = $val['price'];
        }
        return $tierQtyPice;
    }

    /*
     * @return Product Collection
     */
    private function getProductCollection()
    {
        $storeId = $this->storeManager->getStore()->getId();
        return $this->_productCollectionFactory->create()->setStoreId($storeId);
    }

    /*
     *  Get Setup Qty based on Logo, Print sides and Artwork is different or same
     *  on all sides.
     */
    public function getSetupQtyTotal($product, $customPost)
    {
        $skuRB = strpos($product->getSku(), 'RB');
        $front_logo = $customPost['front_color']['value'];
        $back_logo = $customPost['back_color']['value'];
        $side1_logo = $customPost['side1_color']['value'];
        $side2_logo = $customPost['side2_color']['value'];
        $artwork = $customPost['artwork_logos']['value'];
        $print_sides = $customPost['print_sides']['value'];

        $isImprint = strpos(strtolower($front_logo), 'imprint');
        $isBackImprint = strpos(strtolower($back_logo), 'imprint');
        $isSide1Imprint = strpos(strtolower($side1_logo), 'imprint');
        $isSide2Imprint = strpos(strtolower($side2_logo), 'imprint');

        if (!empty($artwork)) { /* Different Artwork of both Sides */
            if (!empty($front_logo) && $isImprint === false) {
                if ($front_logo == '4 or more') {

                    if ($skuRB !== false) {
                        $setupQty = 4; /* RB Series product have setup Qty when logo color '4 or more' */
                    } else {
                        $setupQty = 1;
                    }

                } else {
                    $setupQty = $front_logo;
                }
            } else {
                if ($print_sides == 3) {
                    $setupQty = $print_sides + 1;
                } else {
                    $setupQty = $print_sides;
                }
            }
            if (!empty($back_logo) && $isBackImprint === false) {

                if ($back_logo == '4 or more') {

                    if ($skuRB !== false) {
                        $setupQty += 4;
                    } else {
                        $setupQty++;
                    }

                } else {
                    $setupQty += $back_logo;
                }

            }/*else {
                if($print_sides == 3 ){
                    $setupQty = $print_sides+1;
                } else{
                    $setupQty = $print_sides;
                }
           }*/
            if (!empty($side1_logo) && $isSide1Imprint === false) {
                if ($side1_logo == '4 or more') {
                    if ($skuRB !== false) {
                        $setupQty += 4;
                    } else {
                        $setupQty ++;
                    }
                } else {
                    $setupQty += $side1_logo;
                }
            } /*else {
                if($print_sides == 3 ){
                    $setupQty = $print_sides+1;
                } else{
                    $setupQty = $print_sides;
                }
           }*/
            if (!empty($side2_logo) && $isSide2Imprint === false) {
                if ($side2_logo == '4 or more') {
                    if ($skuRB !== false) {
                        $setupQty += 4;
                    } else {
                        $setupQty ++;
                    }
                } else {
                    $setupQty += $side2_logo;
                }

            }/*else {
                if($print_sides == 3 ){
                    $setupQty = $print_sides+1;
                } else{
                    $setupQty = $print_sides;
                }
           }*/
        } else {  /* Same Artwork of both Sides */

            if (!empty($front_logo) && $isImprint === false) {

                if ($front_logo == '4 or more') {
                    if ($skuRB !== false) {
                        $setupQty = 4; /* RB Series product have setup Qty when logo color '4 or more' */
                    } else {
                        $setupQty = 1;
                    }
                } else {

                    if ($print_sides == 3) {
                        $setupQty = 2 * $front_logo;
                    } else {
                        $setupQty = $front_logo;
                    }

                }

            } else {
                if ($print_sides == 3) {
                    $setupQty = $print_sides + 1;
                } else {
                    $setupQty = $print_sides;
                }
            }
        }
        return $setupQty;
    }

    /*
     *  Get Total of Addtional Imprint Color Price based
     *  on any print sides and logo Colors. It's also depends with Artwork same or different.
     */
    private function getAdditionalImprintPriceTotal($item, $customPost)
    {
        $imprintPriceTotal = 0.00;
        $front_logo = $customPost['front_color']['value'];
        $back_logo = $customPost['back_color']['value'];
        $side1_logo = $customPost['side1_color']['value'];
        $side2_logo = $customPost['side2_color']['value'];
        $artwork = $customPost['artwork_logos']['value'];
        $print_sides = $customPost['print_sides']['value'];
        $isImprint = strpos(strtolower($front_logo), 'imprint');
        $item->setSecondImprintColorTotal($imprintPriceTotal);
        $item->setThirdImprintColorTotal($imprintPriceTotal);
        $item->setFirstSideSecondImprintColorTotal($imprintPriceTotal);
        $item->setFirstSideThirdImprintColorTotal($imprintPriceTotal);
        $item->setSecondSideFirstImprintColorTotal($imprintPriceTotal);
        $item->setSecondSideSecondImprintColorTotal($imprintPriceTotal);
        $item->setSecondSideThirdImprintColorTotal($imprintPriceTotal);
        $item->setThirdSideFirstImprintColorTotal($imprintPriceTotal);
        $item->setThirdSideSecondImprintColorTotal($imprintPriceTotal);
        $item->setThirdSideThirdImprintColorTotal($imprintPriceTotal);
        $item->setFourthSideFirstImprintColorTotal($imprintPriceTotal);
        $item->setFourthSideImprintColorTotal($imprintPriceTotal);
        $item->setFourthSideThirdImprintColorTotal($imprintPriceTotal);

        $imprintPriceTotal = $item->getQty() * $item->getColorPrice();

        if ($print_sides == 1 && $front_logo == 3 || $print_sides == 3 && $front_logo == 1) {
            $item->setThirdImprintColorTotal($imprintPriceTotal);
        }
        if (($print_sides == 1 && in_array($front_logo, [2, 3])) ||
            ($print_sides == 2 && $isImprint !== false) ||
            $print_sides == 3 && $front_logo == 1
        ) {
            $item->setSecondImprintColorTotal($imprintPriceTotal);
        }
        if ($print_sides == 2 && empty($front_logo) || $print_sides == 2 && $front_logo == 1) {
            $item->setSecondSideFirstImprintColorTotal($imprintPriceTotal);
        }
        if (!empty($artwork)) { /* Different Artwork on each sides */
            /* Front Logo Addtional Price Total */
            if ($print_sides == 2 && empty($front_logo)) {
                $item->setSecondSideFirstImprintColorTotal($imprintPriceTotal);
            }
            if (in_array($print_sides, [2, 3]) && in_array($front_logo, [2, 3])) {
                $item->setFirstSideSecondImprintColorTotal($imprintPriceTotal);
            }
            if (in_array($print_sides, [2, 3]) && $front_logo == 3) {
                $item->setFirstSideThirdImprintColorTotal($imprintPriceTotal);
            }
            /* Back Logo Addtional Price Total */
            if (in_array($print_sides, [2, 3]) && in_array($back_logo, [1, 2, 3])) {
                $item->setSecondSideFirstImprintColorTotal($imprintPriceTotal);
            }
            if (in_array($print_sides, [2, 3]) && in_array($back_logo, [2, 3])) {
                $item->setSecondSideSecondImprintColorTotal($imprintPriceTotal);
            }
            if (in_array($print_sides, [2, 3]) && $back_logo == 3) {
                $item->setSecondSideThirdImprintColorTotal($imprintPriceTotal);
            }

            /* Side First Logo Addtional Price Total */
            if ($print_sides == 3 && in_array($side1_logo, [1, 2, 3])) {
                $item->setThirdSideFirstImprintColorTotal($imprintPriceTotal);
            }
            if ($print_sides == 3 && in_array($side1_logo, [2, 3])) {
                $item->setThirdSideSecondImprintColorTotal($imprintPriceTotal);
            }
            if ($print_sides == 3 && $side1_logo == 3) {
                $item->setThirdSideThirdImprintColorTotal($imprintPriceTotal);
            }

            /* Side Two Logo Addtional Price Total */
            if ($print_sides == 3 && in_array($side2_logo, [1, 2, 3])) {
                $item->setFourthSideFirstImprintColorTotal($imprintPriceTotal);
            }
            if ($print_sides == 3 && in_array($side2_logo, [2, 3])) {
                $item->setFourthSideSecondImprintColorTotal($imprintPriceTotal);
            }
            if ($print_sides == 3 && $side2_logo == 3) {
                $item->setFourthSideThirdImprintColorTotal($imprintPriceTotal);
            }

        } else {  /* Same Artwork on each sides */
            if ($print_sides == 2 && $front_logo == 1) {
                $item->setSecondSideFirstImprintColorTotal($imprintPriceTotal);
            }
            if (in_array($print_sides, [2, 3]) && in_array($front_logo, [2, 3])) {
                $item->setFirstSideSecondImprintColorTotal($imprintPriceTotal);

                $item->setSecondSideFirstImprintColorTotal($imprintPriceTotal);
                $item->setSecondSideSecondImprintColorTotal($imprintPriceTotal);
            }
            if ($print_sides == 3 && in_array($front_logo, [2, 3])) {
                $item->setThirdSideFirstImprintColorTotal($imprintPriceTotal);
                $item->setThirdSideSecondImprintColorTotal($imprintPriceTotal);
            }

            if (in_array($print_sides, [2, 3]) && $front_logo == 3) {
                $item->setFirstSideThirdImprintColorTotal($imprintPriceTotal);
                $item->setSecondSideThirdImprintColorTotal($imprintPriceTotal);
            }
            if ($print_sides == 3 && $front_logo == 3) {
                $item->setThirdSideThirdImprintColorTotal($imprintPriceTotal);
            }
        }
    }

    /*
     *  Get Total of Full Imprint Color Price based
     *  on any print sides but logo option is "4 or more". It's also depends with Artwork same or different.
     */
    private function getFullImprintPriceTotal($item, $product, $customPost)
    {
        $fullImprintTotal = 0.00;
        $sku = strpos($product->getSku(), 'RB');
        $front_logo = $customPost['front_color']['value'];
        $back_logo = $customPost['back_color']['value'];
        $side1_logo = $customPost['side1_color']['value'];
        $side2_logo = $customPost['side2_color']['value'];
        $artwork = $customPost['artwork_logos']['value'];
        $print_sides = $customPost['print_sides']['value'];
        $item->setFullImprintColorTotal($fullImprintTotal);
        $item->setFullImprintColorFront($fullImprintTotal);
        $item->setFullImprintColorBack($fullImprintTotal);
        $item->setFullImprintColorFirstSide($fullImprintTotal);
        $item->setFullImprintColorSecondSide($fullImprintTotal);

        $fullImprintTotal = $item->getQty() * $item->getFullColorPrice();

        if ($sku !== false) { /* Full Imprint Total for RB Series */
            if (in_array($print_sides, [1, 2, 3]) && $front_logo == '4 or more' || $back_logo == '4 or more') {
                $item->setFullImprintColorTotal($fullImprintTotal);
            }
        } else {
            if (!empty($artwork)) { /* Artwork Type - Different */
                if (in_array($print_sides, [1, 2, 3]) && $front_logo == '4 or more') {
                    $item->setFullImprintColorFront($fullImprintTotal);
                }
                if (in_array($print_sides, [2, 3]) && $back_logo == '4 or more') {
                    $item->setFullImprintColorBack($fullImprintTotal);
                }
                if ($print_sides == 3 && $side1_logo == '4 or more') {
                    $item->setFullImprintColorFirstSide($fullImprintTotal);
                }
                if ($print_sides == 3 && $side2_logo == '4 or more') {
                    $item->setFullImprintColorSecondSide($fullImprintTotal);
                }
            } else { /* Artwork Type - Same or No */
                if (in_array($print_sides, [1, 2, 3]) && $front_logo == '4 or more') {
                    $item->setFullImprintColorFront($fullImprintTotal);
                }
                if (in_array($print_sides, [2, 3]) && $front_logo == '4 or more') {
                    $item->setFullImprintColorBack($fullImprintTotal);
                }
                if ($print_sides == 3 && $front_logo == '4 or more') {
                    $item->setFullImprintColorFirstSide($fullImprintTotal);
                    $item->setFullImprintColorSecondSide($fullImprintTotal);
                }
            }
        }
    }

    /*
     * return max bulk sample product qty;
     */
    public function sampleProductBulkMaxQty()
    {
        return $this->scopeConfig->getValue(
            self::SAMPLE_MAX_QTY_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
