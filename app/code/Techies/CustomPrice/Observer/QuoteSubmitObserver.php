<?php

namespace Techies\CustomPrice\Observer;

use Magento\Framework\Event\Observer as EventObserver;

class QuoteSubmitObserver implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var TYPESIMPLE
     * retun string
     */
    const TYPESIMPLE=\Magento\Catalog\Model\Product\Type::TYPE_SIMPLE;
    private $quoteItems = [];
    private $quote = null;
    private $order = null;

    /**
     * @var \Magento\Framework\DataObject\Copy
     */
    protected $objectCopyService;

    /**
     * @param \Magento\Framework\DataObject\Copy $objectCopyService
     * ...
     */
    public function __construct(
        \Magento\Framework\DataObject\Copy $objectCopyService
    ) {
        $this->objectCopyService = $objectCopyService;
    }

    /**
     * Add order information into GA block to render on checkout success pages
     *
     * @param EventObserver $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        $this->quote = $observer->getQuote();
        $this->order = $observer->getOrder();
        // can not find a equivalent event for sales_convert_quote_item_to_order_item
        /* @var  \Magento\Sales\Model\Order\Item $orderItem */
        foreach ($this->order->getItems() as $orderItem) {
            if (!$orderItem->getParentItemId() && $orderItem->getProductType() == self::TYPESIMPLE) {
                $quoteItem = $this->getQuoteItemById($orderItem->getQuoteItemId());
                if ($quoteItem) {
                    $additionalOptionsQuote = $quoteItem->getOptionByCode('additional_options');
                    if ($additionalOptionsQuote) {
                        //To do
                        // - check to make sure element are not added twice
                        // - $additionalOptionsQuote - may not be an array
                        $additionalOptionsOrder = $orderItem->getProductOptionByCode('additional_options');
                        if ($additionalOptionsOrder) {
                            $additionalOptions = $additionalOptionsOrder + $additionalOptionsQuote;
                        } else {
                            $additionalOptions = $additionalOptionsQuote;
                        }
                        if ($additionalOptions) {
                            $options = $orderItem->getProductOptions();
                            $options['additional_options'] = (array)json_decode($additionalOptions->getValue());
                            $orderItem->setProductOptions($options);
                        }
                    }
                    $this->objectCopyService->copyFieldsetToTarget(
                        'quote_convert_item',
                        'to_order_item',
                        $quoteItem,
                        $orderItem
                    );
                }
            }
        }
        return $this;
    }

    private function getQuoteItemById($id)
    {
        if (empty($this->quoteItems)) {
            /* @var  \Magento\Quote\Model\Quote\Item $item */
            foreach ($this->quote->getItems() as $item) {
                //filter out config/bundle etc product
                if (!$item->getParentItemId() && $item->getProductType() == self::TYPESIMPLE) {
                    $this->quoteItems[$item->getId()] = $item;
                }
            }
        }
        if (array_key_exists($id, $this->quoteItems)) {
            return $this->quoteItems[$id];
        }
        return null;
    }
}
