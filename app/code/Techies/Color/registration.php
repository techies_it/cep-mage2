<?php
/**
 * Techies India Inc
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Techies_Color',
    __DIR__
);
