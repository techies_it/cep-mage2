<?php
/**
 * Techies India Inc
 */

namespace Techies\Color\Api;

use Techies\Color\Api\Data\ColorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * @api
 */
interface ColorRepositoryInterface
{
    /**
     * @param ColorInterface $Color
     * @return ColorInterface
     */
    public function save(ColorInterface $Color);

    /**
     * @param $id
     * @return ColorInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($id);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Techies\Color\Api\Data\ColorSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param ColorInterface $Color
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(ColorInterface $Color);

    /**
     * @param int $ColorId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($ColorId);

    /**
     * clear caches instances
     * @return void
     */
    public function clear();
}
