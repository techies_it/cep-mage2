<?php
/**
 * Techies India Inc
 */

namespace Techies\Color\Api;

/**
 * @api
 */
interface ExecutorInterface
{
    /**
     * execute
     * @param int $id
     */
    public function execute($id);
}
