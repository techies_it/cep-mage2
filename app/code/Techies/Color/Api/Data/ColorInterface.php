<?php
/**
 * Techies India Inc
 */

namespace Techies\Color\Api\Data;

/**
 * @api
 */
interface ColorInterface
{
    const COLOR_ID = 'color_id';
    const COLOR_NAME = 'color_name';
    const COLOR_CODE = 'color_code';
    const COLOR_PMS = 'color_pms';
    /**
     * @var string
     */
    const IS_ACTIVE = 'is_active';
    /**
     * @var int
     */
    const STATUS_ENABLED = 1;
    /**
     * @var int
     */
    const STATUS_DISABLED = 2;
    /**
     * @param int $id
     * @return ColorInterface
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return ColorInterface
     */
    public function setColorId($id);

    /**
     * @return int
     */
    public function getColorId();

    /**
     * @param string $colorName
     * @return ColorInterface
     */
    public function setColorName($colorName);

    /**
     * @return string
     */
    public function getColorName();
    /**
     * @param string $colorCode
     * @return ColorInterface
     */
    public function setColorCode($colorCode);

    /**
     * @return string
     */
    public function getColorCode();
    /**
     * @param string $colorPms
     * @return ColorInterface
     */
    public function setColorPms($colorPms);

    /**
     * @return string
     */
    public function getColorPms();
    /**
     * @param int $isActive
     * @return ColorInterface
     */
    public function setIsActive($isActive);

    /**
     * @return int
     */
    public function getIsActive();
}
