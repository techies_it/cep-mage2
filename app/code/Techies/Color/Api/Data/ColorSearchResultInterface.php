<?php
/**
 * Techies India Inc
 */

namespace Techies\Color\Api\Data;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * @api
 */
interface ColorSearchResultInterface
{
    /**
     * get items
     *
     * @return \Techies\Color\Api\Data\ColorInterface[]
     */
    public function getItems();

    /**
     * Set items
     *
     * @param \Techies\Color\Api\Data\ColorInterface[] $items
     * @return $this
     */
    public function setItems(array $items);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return $this
     */
    public function setSearchCriteria(SearchCriteriaInterface $searchCriteria);

    /**
     * @param int $count
     * @return $this
     */
    public function setTotalCount($count);
}
