<?php
/**
 * Techies India Inc
 */

namespace Techies\Color\Model\ResourceModel\Color;

use Techies\Color\Model\Color;
use Techies\Color\Model\ResourceModel\AbstractCollection;

/**
 * @api
 */
class Collection extends AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Color::class,
            \Techies\Color\Model\ResourceModel\Color::class
        );
    }
}
