<?php
/**
 * Techies India Inc
 */

namespace Techies\Color\Model\ResourceModel;

class Color extends AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('techies_color_color', 'color_id');
    }
}
