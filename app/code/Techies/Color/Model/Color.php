<?php
/**
 * Techies India Inc
 */

namespace Techies\Color\Model;

use Techies\Color\Api\Data\ColorInterface;
use Magento\Framework\Model\AbstractModel;
use Techies\Color\Model\ResourceModel\Color as ColorResourceModel;

/**
 * @method \Techies\Color\Model\ResourceModel\Page _getResource()
 * @method \Techies\Color\Model\ResourceModel\Page getResource()
 */
class Color extends AbstractModel implements ColorInterface
{
    /**
     * this is user for Cache tag
     * in block xml file
     *
     * @var string
     */
    const CACHE_TAG = 'techies_color_color';
    /**
     * Cache tag
     *
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;
    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'techies_color_color';
    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'color';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ColorResourceModel::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get Page id
     *
     * @return array
     */
    public function getColorId()
    {
        return $this->getData(ColorInterface::COLOR_ID);
    }

    /**
     * set color id
     *
     * @param int $colorId
     * @return ColorInterface
     */
    public function setColorId($colorId)
    {
        return $this->setData(ColorInterface::COLOR_ID, $colorId);
    }

    /**
     * @param string $colorName
     * @return ColorInterface
     */
    public function setColorName($colorName)
    {
        return $this->setData(ColorInterface::COLOR_NAME, $colorName);
    }

    /**
     * @return string
     */
    public function getColorName()
    {
        return $this->getData(ColorInterface::COLOR_NAME);
    }

    /**
     * @param string $colorCode
     * @return ColorInterface
     */
    public function setColorCode($colorCode)
    {
        return $this->setData(ColorInterface::COLOR_CODE, $colorCode);
    }

    /**
     * @return string
     */
    public function getColorCode()
    {
        return $this->getData(ColorInterface::COLOR_CODE);
    }

    /**
     * @param string $colorPms
     * @return ColorInterface
     */
    public function setColorPms($colorPms)
    {
        return $this->setData(ColorInterface::COLOR_PMS, $colorPms);
    }

    /**
     * @return string
     */
    public function getColorPms()
    {
        return $this->getData(ColorInterface::COLOR_PMS);
    }

    /**
     * @param int $isActive
     * @return ColorInterface
     */
    public function setIsActive($isActive)
    {
        return $this->setData(ColorInterface::IS_ACTIVE, $isActive);
    }

    /**
     * @return int
     */
    public function getIsActive()
    {
        return $this->getData(ColorInterface::IS_ACTIVE);
    }
}
