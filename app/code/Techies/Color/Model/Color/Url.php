<?php
/**
 * Techies India Inc
 */


namespace Techies\Color\Model\Color;

use Magento\Framework\UrlInterface;
use Techies\Color\Api\Data\ColorInterface;

class Url
{
    /**
     * url builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;
    /**
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        UrlInterface $urlBuilder
    ) {
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * @return string
     */
    public function getListUrl()
    {
        return $this->urlBuilder->getUrl('techies_color/color/index');
    }
}
