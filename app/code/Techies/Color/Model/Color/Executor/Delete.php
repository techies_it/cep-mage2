<?php
/**
 * Techies India Inc
 */

namespace Techies\Color\Model\Color\Executor;

use Techies\Color\Api\ColorRepositoryInterface;
use Techies\Color\Api\ExecutorInterface;

class Delete implements ExecutorInterface
{
    /**
     * @var ColorRepositoryInterface
     */
    private $colorRepository;

    /**
     * Delete constructor.
     * @param ColorRepositoryInterface $colorRepository
     */
    public function __construct(
        ColorRepositoryInterface $colorRepository
    ) {
        $this->colorRepository = $colorRepository;
    }

    /**
     * @param int $id
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute($id)
    {
        $this->colorRepository->deleteById($id);
    }
}
