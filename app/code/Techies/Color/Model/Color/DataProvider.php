<?php
/**
 * Techies India Inc
 */

namespace Techies\Color\Model\Color;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Techies\Color\Model\ResourceModel\Color\CollectionFactory as ColorCollectionFactory;

class DataProvider extends AbstractDataProvider
{
    /**
     * Loaded data cache
     *
     * @var array
     */
    protected $loadedData;

    /**
     * Data persistor
     *
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param ColorCollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        ColorCollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var \Techies\Color\Model\Color $color */
        foreach ($items as $color) {
            $this->loadedData[$color->getId()] = $color->getData();
        }
        $data = $this->dataPersistor->get('techies_color_color');
        if (!empty($data)) {
            $color = $this->collection->getNewEmptyItem();
            $color->setData($data);
            $this->loadedData[$color->getId()] = $color->getData();
            $this->dataPersistor->clear('techies_color_color');
        }
        return $this->loadedData;
    }
}
