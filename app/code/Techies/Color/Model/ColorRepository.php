<?php
/**
 * Techies India Inc
 */

namespace Techies\Color\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\ValidatorException;
use Techies\Color\Api\Data\ColorInterface;
use Techies\Color\Api\Data\ColorInterfaceFactory;
use Techies\Color\Api\Data\ColorSearchResultInterfaceFactory;
use Techies\Color\Api\ColorRepositoryInterface;
use Techies\Color\Model\ResourceModel\Color as ColorResourceModel;
use Techies\Color\Model\ResourceModel\Color\Collection;
use Techies\Color\Model\ResourceModel\Color\CollectionFactory as ColorCollectionFactory;

class ColorRepository implements ColorRepositoryInterface
{
    /**
     * Cached instances
     *
     * @var array
     */
    protected $instances = [];

    /**
     * Color resource model
     *
     * @var ColorResourceModel
     */
    protected $resource;

    /**
     * Color collection factory
     *
     * @var ColorCollectionFactory
     */
    protected $colorCollectionFactory;

    /**
     * Color interface factory
     *
     * @var ColorInterfaceFactory
     */
    protected $colorInterfaceFactory;

    /**
     * Data Object Helper
     *
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * Search result factory
     *
     * @var ColorSearchResultInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * constructor
     * @param ColorResourceModel $resource
     * @param ColorCollectionFactory $colorCollectionFactory
     * @param ColornterfaceFactory $colorInterfaceFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param ColorSearchResultInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        ColorResourceModel $resource,
        ColorCollectionFactory $colorCollectionFactory,
        ColorInterfaceFactory $colorInterfaceFactory,
        DataObjectHelper $dataObjectHelper,
        ColorSearchResultInterfaceFactory $searchResultsFactory
    ) {
        $this->resource             = $resource;
        $this->colorCollectionFactory = $colorCollectionFactory;
        $this->colorInterfaceFactory  = $colorInterfaceFactory;
        $this->dataObjectHelper     = $dataObjectHelper;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * Save Color.
     *
     * @param \Techies\Color\Api\Data\ColorInterface $color
     * @return \Techies\Color\Api\Data\ColorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(ColorInterface $color)
    {
        /** @var ColorInterface|\Magento\Framework\Model\AbstractModel $color */
        try {
            $this->resource->save($color);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the Color: %1',
                $exception->getMessage()
            ));
        }
        return $color;
    }

    /**
     * Retrieve Color
     *
     * @param int $colorId
     * @return \Techies\Color\Api\Data\ColorInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($colorId)
    {
        if (!isset($this->instances[$colorId])) {
            /** @var ColorInterface|\Magento\Framework\Model\AbstractModel $color */
            $color = $this->colorInterfaceFactory->create();
            $this->resource->load($color, $colorId);
            if (!$color->getId()) {
                throw new NoSuchEntityException(__('Requested Color doesn\'t exist'));
            }
            $this->instances[$colorId] = $color;
        }
        return $this->instances[$colorId];
    }

    /**
     * Retrieve Colors matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Techies\Color\Api\Data\ColorSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Techies\Color\Api\Data\ColorSearchResultInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Techies\Color\Model\ResourceModel\Color\Collection $collection */
        $collection = $this->colorCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var \Magento\Framework\Api\Search\FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? SortOrder::SORT_ASC : SortOrder::SORT_DESC
                );
            }
        } else {
            $collection->addOrder('main_table.' . ColorInterface::COLOR_ID, SortOrder::SORT_ASC);
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var ColorInterface[] $colors */
        $colors = [];
        /** @var \Techies\Color\Model\Color $color */
        foreach ($collection as $color) {
            /** @var ColorInterface $colorDataObject */
            $colorDataObject = $this->colorInterfaceFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $colorDataObject,
                $color->getData(),
                ColorInterface::class
            );
            $colors[] = $colorDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($colors);
    }

    /**
     * Delete Color
     *
     * @param ColorInterface $color
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(ColorInterface $color)
    {
        /** @var ColorInterface|\Magento\Framework\Model\AbstractModel $color */
        $id = $color->getId();
        try {
            unset($this->instances[$id]);
            $this->resource->delete($color);
        } catch (ValidatorException $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new StateException(
                __('Unable to removeColor %1', $id)
            );
        }
        unset($this->instances[$id]);
        return true;
    }

    /**
     * Delete Color by ID.
     *
     * @param int $colorId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($colorId)
    {
        $color = $this->get($colorId);
        return $this->delete($color);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param FilterGroup $filterGroup
     * @param Collection $collection
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function addFilterGroupToCollection(
        FilterGroup $filterGroup,
        Collection $collection
    ) {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }

    /**
     * clear caches instances
     * @return void
     */
    public function clear()
    {
        $this->instances = [];
    }
}
