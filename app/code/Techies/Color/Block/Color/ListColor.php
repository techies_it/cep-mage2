<?php
/**
 * Techies India Inc
 */


namespace Techies\Color\Block\Color;

use Magento\Framework\UrlFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Theme\Block\Html\Pager;
use Techies\Color\Api\Data\ColorInterface;
use Techies\Color\Model\Color;
use Techies\Color\Model\ResourceModel\Color\CollectionFactory as ColorCollectionFactory;

/**
 * @api
 */
class ListColor extends Template
{
    /**
     * @var ColorCollectionFactory
     */
    private $colorCollectionFactory;
    /**
     * @var \Techies\Color\Model\ResourceModel\Color\Collection
     */
    private $colors;

    /**
     * @param Context $context
     * @param ColorCollectionFactory $colorCollectionFactory
     * @param array $data
     */
    public function __construct(Context $context, ColorCollectionFactory $colorCollectionFactory, array $data = [])
    {
        $this->colorCollectionFactory = $colorCollectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return \Techies\Color\Model\ResourceModel\Color\Collection
     */
    public function getColors()
    {
        if ($this->colors===null) {
            $this->colors = $this->colorCollectionFactory->create()
                ->addFieldToFilter('is_active', ColorInterface::STATUS_ENABLED)
                ->setOrder('color_name', 'ASC');
        }
        return $this->colors;
    }

    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        /** @var \Magento\Theme\Block\Html\Pager $pager */
        $pager = $this->getLayout()->createBlock(Pager::class, 'techies.color.color.list.pager');
        $pager->setCollection($this->getColors());
        $this->setChild('pager', $pager);
        return $this;
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}
