<?php
/**
 * Techies India Inc
 */

namespace Techies\Color\Block\Adminhtml\Button\Color;

use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class Delete implements ButtonProviderInterface
{
    /**
     * @var Registry
     */
    private $registry;
    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * Delete constructor.
     * @param Registry $registry
     * @param UrlInterface $url
     */
    public function __construct(Registry $registry, UrlInterface $url)
    {
        $this->registry = $registry;
        $this->url = $url;
    }

    /**
     * get button data
     *
     * @return array
     */
    public function getButtonData()
    {
        $data = [];
        if ($this->getColorId()) {
            $data = [
                'label' => __('Delete color'),
                'class' => 'delete',
                'on_click' => 'deleteConfirm(\'' . __(
                    'Are you sure you want to do this?'
                ) . '\', \'' . $this->getDeleteUrl() . '\')',
                'sort_order' => 20,
            ];
        }
        return $data;
    }

    /**
     * @return \Techies\Color\Api\Data\ColorInterface | null
     */
    private function getColor()
    {
        return $this->registry->registry('current_color');
    }

    /**
     * @return int|null
     */
    private function getColorId()
    {
        $color = $this->getColor();
        return ($color) ? $color->getId() : null;
    }

    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        return $this->url->getUrl(
            '*/*/delete',
            [
                'color_id' => $this->getcolorId()
            ]
        );
    }
}
