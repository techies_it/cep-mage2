<?php
namespace Techies\Color\Ui\Provider;

interface CollectionProviderInterface
{
    /**
     * @return \Techies\Color\Model\ResourceModel\AbstractCollection
     */
    public function getCollection();
}
