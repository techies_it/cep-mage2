<?php
/**
 * Techies India Inc
 */

namespace Techies\Color\Test\Unit\Block\Adminhtml\Button\Color;

use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use PHPUnit\Framework\TestCase;
use Techies\Color\Api\Data\ColorInterface;
use Techies\Color\Block\Adminhtml\Button\Color\Delete;

class DeleteTest extends TestCase
{
    /**
     * @var UrlInterface | \PHPUnit_Framework_MockObject_MockObject
     */
    private $url;
    /**
     * @var Registry | \PHPUnit_Framework_MockObject_MockObject
     */
    private $registry;
    /**
     * @var Delete
     */
    private $button;

    /**
     * set up tests
     */
    protected function setUp()
    {
        $this->url = $this->createMock(UrlInterface::class);
        $this->registry = $this->createMock(Registry::class);
        $this->button = new Delete($this->registry, $this->url);
    }

    /**
     * @covers \Techies\Color\Block\Adminhtml\Button\Color\Delete::getButtonData()
     */
    public function testButtonDataNoColor()
    {
        $this->registry->method('registry')->willReturn(null);
        $this->url->expects($this->exactly(0))->method('getUrl');
        $this->assertEquals([], $this->button->getButtonData());
    }

    /**
     * @covers \Techies\Color\Block\Adminhtml\Button\Color\Delete::getButtonData()
     */
    public function testButtonDataNoColorId()
    {
        $color = $this->createMock(ColorInterface::class);
        $color->method('getId')->willReturn(null);
        $this->registry->method('registry')->willReturn($color);
        $this->url->expects($this->exactly(0))->method('getUrl');
        $this->assertEquals([], $this->button->getButtonData());
    }

    /**
     * @covers \Techies\Color\Block\Adminhtml\Button\Color\Delete::getButtonData()
     */
    public function testButtonData()
    {
        $color = $this->createMock(ColorInterface::class);
        $color->method('getId')->willReturn(2);
        $this->registry->method('registry')->willReturn($color);
        $this->url->expects($this->once())->method('getUrl');
        $data = $this->button->getButtonData();
        $this->assertArrayHasKey('on_click', $data);
        $this->assertArrayHasKey('label', $data);
        $this->assertArrayHasKey('class', $data);
    }
}
