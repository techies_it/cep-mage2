<?php
/**
 * Techies India Inc
 */

namespace Techies\Color\Test\Unit\Model\Color\Executor;

use PHPUnit\Framework\TestCase;
use Techies\Color\Api\ColorRepositoryInterface;
use Techies\Color\Api\Data\ColorInterface;
use Techies\Color\Model\Color\Executor\Delete;

class DeleteTest extends TestCase
{
    /**
     * @covers \Techies\Color\Model\Color\Executor\Delete::execute()
     */
    public function testExecute()
    {
        /** @var ColorRepositoryInterface | \PHPUnit_Framework_MockObject_MockObject $colorRepository */
        $colorRepository = $this->createMock(ColorRepositoryInterface::class);
        $colorRepository->expects($this->once())->method('deleteById');
        /** @var ColorInterface | \PHPUnit_Framework_MockObject_MockObject $color */
        $color = $this->createMock(ColorInterface::class);
        $delete = new Delete($colorRepository);
        $delete->execute($color->getId());
    }
}
