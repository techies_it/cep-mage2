<?php
/**
 * Techies India Inc
 */

namespace Techies\Color\Controller\Adminhtml\Color;

use Techies\Color\Api\ColorRepositoryInterface;
use Techies\Color\Api\Data\ColorInterface;
use Techies\Color\Model\ResourceModel\Color as ColorResourceModel;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Reflection\DataObjectProcessor;

/**
 * Class InlineEdit
 * This class use for edit data inline on grid and save
 */
class InlineEdit extends Action
{
    /**
     * Color repository
     * @var ColorRepositoryInterface
     */
    protected $colorRepository;
    /**
     * Data object processor
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;
    /**
     * Data object helper
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;
    /**
     * JSON Factory
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * Color resource model
     * @var ColorResourceModel
     */
    protected $colorResourceModel;

    /**
     * constructor
     * @param Context $context
     * @param ColorRepositoryInterface $colorRepository
     * @param DataObjectProcessor $dataObjectProcessor
     * @param DataObjectHelper $dataObjectHelper
     * @param JsonFactory $jsonFactory
     * @param ColorResourceModel $colorResourceModel
     */
    public function __construct(
        Context $context,
        ColorRepositoryInterface $colorRepository,
        DataObjectProcessor $dataObjectProcessor,
        DataObjectHelper $dataObjectHelper,
        JsonFactory $jsonFactory,
        ColorResourceModel $colorResourceModel
    ) {
        $this->colorRepository = $colorRepository;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->jsonFactory = $jsonFactory;
        $this->colorResourceModel = $colorResourceModel;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData([
                'messages' => [__('Please correct the data sent.')],
                'error' => true,
            ]);
        }

        foreach (array_keys($postItems) as $colorId) {
            /** @var \Techies\Color\Model\Color|\Techies\Color\Api\Data\ColorInterface $color */
            try {
                $color = $this->colorRepository->get((int)$colorId);
                $colorData = $postItems[$colorId];
                $this->dataObjectHelper->populateWithArray($color, $colorData, ColorInterface::class);
                $this->colorResourceModel->saveAttribute($color, array_keys($colorData));
            } catch (LocalizedException $e) {
                $messages[] = $this->getErrorWithColorId($color, $e->getMessage());
                $error = true;
            } catch (\RuntimeException $e) {
                $messages[] = $this->getErrorWithColorId($color, $e->getMessage());
                $error = true;
            } catch (\Exception $e) {
                $messages[] = $this->getErrorWithColorId(
                    $color,
                    __('Something went wrong while saving the Color.')
                );
                $error = true;
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * Add Color id to error message
     *
     * @param \Techies\Color\Api\Data\ColorInterface $color
     * @param string $errorText
     * @return string
     */
    protected function getErrorWithColorId(ColorInterface $color, $errorText)
    {
        return '[Color ID: ' . $color->getId() . '] ' . $errorText;
    }
}
