<?php
/**
 * Techies India Inc
 */

namespace Techies\Color\Controller\Adminhtml\Color;

use Techies\Color\Api\ColorRepositoryInterface;
use Techies\Color\Api\Data\ColorInterface;
use Techies\Color\Api\Data\ColorInterfaceFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Registry;

/**
 * Class Save
 * This class use for save color in action class
 */
class Save extends Action
{
    /**
     * Color factory
     * @var ColorInterfaceFactory
     */
    protected $colorFactory;
    /**
     * Data Object Processor
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;
    /**
     * Data Object Helper
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;
    /**
     * Data Persistor
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    /**
     * Core registry
     * @var Registry
     */
    protected $registry;
    /**
     * Color repository
     * @var ColorRepositoryInterface
     */
    protected $colorRepository;

    /**
     * Save constructor.
     * @param Context $context
     * @param ColorInterfaceFactory $colorFactory
     * @param ColorRepositoryInterface $colorRepository
     * @param DataObjectProcessor $dataObjectProcessor
     * @param DataObjectHelper $dataObjectHelper
     * @param DataPersistorInterface $dataPersistor
     * @param Registry $registry
     */
    public function __construct(
        Context $context,
        ColorInterfaceFactory $colorFactory,
        ColorRepositoryInterface $colorRepository,
        DataObjectProcessor $dataObjectProcessor,
        DataObjectHelper $dataObjectHelper,
        DataPersistorInterface $dataPersistor,
        Registry $registry
    ) {
        $this->colorFactory = $colorFactory;
        $this->colorRepository = $colorRepository;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataPersistor = $dataPersistor;
        $this->registry = $registry;
        parent::__construct($context);
    }

    /**
     * run the action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var ColorInterface $color */
        $color = null;
        $postData = $this->getRequest()->getPostValue();
        $data = $postData;
        $id = !empty($data['color_id']) ? $data['color_id'] : null;
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            if ($id) {
                $color = $this->colorRepository->get((int)$id);
            } else {
                unset($data['color_id']);
                $color = $this->colorFactory->create();
            }
            $this->dataObjectHelper->populateWithArray($color, $data, ColorInterface::class);
            $this->colorRepository->save($color);
            $this->messageManager->addSuccessMessage(__('You saved the color'));
            $this->dataPersistor->clear('techies_color_color');
            if ($this->getRequest()->getParam('back')) {
                $resultRedirect->setPath('*/*/edit', ['color_id' => $color->getId()]);
            } else {
                $resultRedirect->setPath('*/*');
            }
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->dataPersistor->set('techies_color_color', $postData);
            $resultRedirect->setPath('*/*/edit', ['color_id' => $id]);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was a problem saving the color'));
            $this->dataPersistor->set('techies\color_color', $postData);
            $resultRedirect->setPath('*/*/edit', ['color_id' => $id]);
        }
        return $resultRedirect;
    }
}
