<?php
/**
 * Techies India Inc
 */

namespace Techies\Color\Controller\Adminhtml\Color;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Techies\Color\Api\ColorRepositoryInterface;

class Edit extends Action
{
    /**
     * @var ColorRepositoryInterface
     */
    private $colorRepository;
    /**
     * @var Registry
     */
    private $registry;

    /**
     * Edit constructor.
     * @param Context $context
     * @param ColorRepositoryInterface $colorRepository
     * @param Registry $registry
     */
    public function __construct(
        Context $context,
        ColorRepositoryInterface $colorRepository,
        Registry $registry
    ) {
        $this->colorRepository = $colorRepository;
        $this->registry = $registry;
        parent::__construct($context);
    }

    /**
     * get current color
     *
     * @return null|\Techies\Color\Api\Data\ColorInterface
     */
    private function initColor()
    {
        $colorId = $this->getRequest()->getParam('color_id');
        try {
            $color = $this->colorRepository->get($colorId);
        } catch (NoSuchEntityException $e) {
            $color = null;
        }
        $this->registry->register('current_color', $color);
        return $color;
    }

    /**
     * Edit or create color
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $color = $this->initColor();
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu('Techies_Color::color_color');
        $resultPage->getConfig()->getTitle()->prepend(__('colors'));

        if ($color === null) {
            $resultPage->getConfig()->getTitle()->prepend(__('New color'));
        } else {
            $resultPage->getConfig()->getTitle()->prepend($color->getColorName());
        }
        return $resultPage;
    }
}
