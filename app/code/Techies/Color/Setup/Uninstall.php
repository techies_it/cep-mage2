<?php
/**
 * Techies India Inc
 */

namespace Techies\Color\Setup;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UninstallInterface;

class Uninstall implements UninstallInterface
{
    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * Uninstall constructor.
     * @param ResourceConnection $resource
     */
    public function __construct(ResourceConnection $resource)
    {
        $this->resource = $resource;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.Generic.CodeAnalysis.UnusedFunctionParameter)
     */
    public function uninstall(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        //remove ui bookmark data
        $this->resource->getConnection()->delete(
            $this->resource->getTableName('ui_bookmark'),
            [
                'namespace IN (?)' => [
                    'techies_color_color_listing',
                ]
            ]
        );
        //remove config data
        $this->resource->getConnection()->delete(
            $this->resource->getTableName('core_config_data'),
            [
                'path LIKE ?' => 'techies_color_%'
            ]
        );
        if ($setup->tableExists('techies_color_color')) {
            $setup->getConnection()->dropTable('techies_color_color');
        }
    }
}
