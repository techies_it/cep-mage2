<?php
namespace Techies\CatalogImageResize\Block\Rewrite\Product;

use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class ImageResize
 * This is useable for resize product image to get image url without cache in path.
 */
class ImageResize extends Template
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var \Magento\Framework\Image\AdapterFactory
     */
    protected $imageFactory;
    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $filesystem;
    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $directory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Image\AdapterFactory $imageFactory,
        \Magento\Framework\Filesystem $filesystem,
        array $data = []
    ) {
        $this->storeManager = $context->getStoreManager();
        $this->imageFactory = $imageFactory;
        $this->filesystem = $filesystem;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        parent::__construct($context, $data);
    }

    /**
     * @param $imageName
     * @param int $width
     * @param int $height
     * @return bool|string
     */
    public function getResizeImage($imageName, $width = 260, $height = 260)
    {
        $imageUrl = false;
        if (isset($imageName) && !empty($imageName)) {
            /* Real path of image from directory */
            $file = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)
                ->getAbsolutePath('catalog/product/'.$width.'X'.$height.$imageName);
            $urlMedia = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            if ($this->directory->isFile($file) && $this->directory->isExist($file)) {
                $imageUrl = $urlMedia . "catalog/product/".$width.'X'.$height.$imageName;
            } else {
                $realPath = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)
                    ->getAbsolutePath('catalog/product'.$imageName);
                if (!$this->directory->isFile($realPath) || !$this->directory->isExist($realPath)) {
                    return false;
                }
                $targetDir = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)
                    ->getAbsolutePath('catalog/product/'.$width.'X'.$height);
                $pathTargetDir = $this->directory->getRelativePath($targetDir);

                if (!$this->directory->isExist($pathTargetDir)) {
                    $this->directory->create($pathTargetDir);
                }
                if (!$this->directory->isExist($pathTargetDir)) {
                    return false;
                }
                $image = $this->imageFactory->create();
                $image->open($realPath);
                $image->keepAspectRatio(true);
                $image->constrainOnly(true);
                $image->keepFrame(false);
                $image->resize($width, $height);
                $dest = $targetDir.$imageName;
                $image->save($dest);
                if ($this->directory->isFile($this->directory->getRelativePath($dest))) {
                    $imageUrl = $urlMedia . 'catalog/product/'.$width.'X'.$height.$imageName;
                }
            }
        }
        return $imageUrl;
    }
}
