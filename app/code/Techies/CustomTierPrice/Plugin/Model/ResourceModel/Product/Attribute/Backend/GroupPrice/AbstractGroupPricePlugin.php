<?php

namespace Techies\CustomTierPrice\Plugin\Model\ResourceModel\Product\Attribute\Backend\GroupPrice;

class AbstractGroupPricePlugin
{
    public function afterGetSelect(
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\Backend\GroupPrice\AbstractGroupPrice $subject,
        $result
    ) {

        //add production time column to select
        $result->columns('production_time');
        // logging to test override
        $logger = \Magento\Framework\App\ObjectManager::getInstance()->get(\Psr\Log\LoggerInterface::class);
        $logger->debug('Tier Price for Production' . $result);
        return $result;
    }
}
