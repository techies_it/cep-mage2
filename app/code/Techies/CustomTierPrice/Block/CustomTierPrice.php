<?php

namespace Techies\CustomTierPrice\Block;

use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Product;

class CustomTierPrice extends \Magento\Framework\View\Element\Template
{
    protected $productRepository;
    protected $productCollectionFactory;
    protected $urlHelper;
    protected $storeManager;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context ,
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        array $data = []
    ) {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_productRepository = $productRepository;
        $this->urlHelper = $urlHelper;
        $this->storeManager = $context->getStoreManager();
        parent::__construct($context, $data);
    }

    /*
    * Get Production Time Value via Product ID
    * return @array
    */
    public function getProductionTime($productID)
    {
        $productCollection = $this->getProductCollection();
        $productCollection->addAttributeToSelect('e.entity_id')
            ->addAttributeToFilter('entity_id', ['eq' => $productID]);
        $productCollection->getSelect()->join(
            ['tier' => 'catalog_product_entity_tier_price'],
            'e.entity_id = tier.entity_id',
            ['production_time', 'customer_group_id']
        )->group('production_time');
        $production = $productCollection->getData();
        return $production;
    }

    /*
    * get Lowest Price of Tier Prices
    * @param $productID
    * @return $lowestPice
    */
    public function getLowestTierPrice($productID)
    {
        $productCollection = $this->getProductCollection();
        $productCollection->addAttributeToSelect('e.entity_id')
            ->addAttributeToFilter('entity_id', ['eq' => $productID]);
        $productCollection->getSelect()->joinLeft(
            ['tier' => 'catalog_product_entity_tier_price'],
            'e.entity_id = tier.entity_id',
            ['min(tier.value) as price']
        );
        $tierprices = $productCollection->getData();
        foreach ($tierprices as $key => $val) {
            $tierprice = $val['price'];
        }
        return $tierprice;
    }

    /*
    * @return Product Collection
    */
    public function getProductCollection()
    {
        $storeId = $this->storeManager->getStore()->getId();
        return $this->_productCollectionFactory->create()->setStoreId($storeId);
    }
}
