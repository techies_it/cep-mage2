<?php
namespace Techies\CustomTierPrice\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Form\Fieldset;
use Magento\Ui\Component\Container;
use Magento\Ui\Component\Form\Element\DataType\Number;
use Magento\Ui\Component\Form\Element\DataType\Price;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\Form\Element\Select;
use Magento\Framework\Stdlib\ArrayManager;

class AdvancedPricing extends \Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AdvancedPricing
{

    protected $arrayManager;

    public function __construct(ArrayManager $arrayManager)
    {
        $this->arrayManager = $arrayManager;
    }

    public function modifyMeta(array $meta)
    {
        $tierPricePath = $this->arrayManager->findPath('tier_price', $meta, null, 'children');

        if ($tierPricePath) {
            $meta = $this->arrayManager->merge(
                $tierPricePath,
                $meta,
                $this->getTierPriceStructure()
            );
        }

        return $meta;
    }

    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * Get tier price dynamic rows structure
     *
     * @param string $tierPricePath
     * @return array
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    
    protected function getTierPriceStructure()
    {
        return [
            'children' => [
                'record' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Container::NAME,
                                'isTemplate' => true,
                                'is_collection' => true,
                                'component' => 'Magento_Ui/js/dynamic-rows/record',
                                'dataScope' => '',
                            ],
                        ],
                    ],
                    'children' => [
                        'production_time' => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'formElement' => Select::NAME,
                                        'componentType' => Field::NAME,
                                        'dataType' => Text::NAME,
                                        'dataScope' => 'production_time',
                                        'label' => __('Production Time'),
                                        'options' => [
                                            ['value' => '0', 'label' => '--Select Options--'],
                                            ['value' => '1', 'label' => '2 weeks or less'],
                                            ['value' => '2', 'label' => '3 weeks or less'],
                                            ['value' => '3', 'label' => 'Within 30-45 days'],
                                            ['value' => '4', 'label' => 'Within 60-90 days'],
                                        ],
                                        'sortOrder' => 25,
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
