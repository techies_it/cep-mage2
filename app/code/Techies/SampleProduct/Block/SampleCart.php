<?php
namespace Techies\SampleProduct\Block;

class SampleCart extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    public function getConfig($config)
    {
        return $this->scopeConfig->getValue(
            'sample_product/general/'.$config,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
