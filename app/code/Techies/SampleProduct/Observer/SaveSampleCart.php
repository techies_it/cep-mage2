<?php
namespace Techies\SampleProduct\Observer;

class SaveSampleCart implements \Magento\Framework\Event\ObserverInterface
{
    protected $_request;
    protected $_helper;
    protected $cart;
    private $serializer;
    protected $productCollectionFactory;
    protected $urlHelper;
    protected $storeManager;
    public function __construct(
        \Techies\SampleProduct\Block\SampleCart $sample,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
    ) {
        $this->_request = $request;
        $this->_sample = $sample;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->storeManager = $context->getStoreManager();
    }
    /*
    * Save Sample value in addtional options
    */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /* @var \Magento\Quote\Model\Quote\Item $item */
        $param = $this->_request->getParams();
        $item = $observer->getEvent()->getQuoteItem();
        $additionalOptions = [];
        if ($additionalOption = $item->getOptionByCode('additional_options')) {
            $additionalOptions = (array)  json_decode($additionalOption->getValue());
        }
        $isSample = $this->_request->getParam('sample');
        $price = $this->getLowestTierPrice($item->getProductId());
        if (!empty($isSample)) {
            $item->setIsSample($isSample);
            $additionalOptions[] = [
                'label' => 'Sample',
                'value' => 'yes'
            ];
            if (count($additionalOptions) > 0) {
                $item->addOption([
                    'product_id' => $item->getProductId(),
                    'code' => 'additional_options',
                    'value' => json_encode($additionalOptions)
                ]);
            }
        }
        if ($param['qty'] <= $this->_sample->getConfig('max_qty')) {
            $item->setCustomPrice($price);
            $item->setOriginalCustomPrice($price);
        }
        $item->getProduct()->setIsSuperMode(true);
    }

     /*
    * get Lowest Price of Tier Prices
    * @param $productID
    * @return $lowestPice
    */
    public function getLowestTierPrice($productID)
    {
        $productCollection = $this->getProductCollection();
        $productCollection->addAttributeToSelect('e.entity_id')
        ->addAttributeToFilter('entity_id', ['eq' => $productID]);
        $productCollection->getSelect()->joinLeft(
            ['tier' => 'catalog_product_entity_tier_price'],
            'e.entity_id = tier.entity_id',
            ['min(tier.value) as price']
        );
        $tierprices = $productCollection->getData();
        foreach ($tierprices as $key => $val) {
            $tierprice = $val['price'];
        }
        return $tierprice;
    }

    /*
    * @return Product Collection
    */
    private function getProductCollection()
    {
        $storeId = $this->storeManager->getStore()->getId();
        return $this->_productCollectionFactory->create()->setStoreId($storeId);
    }
}
