<?php
namespace Techies\HomeProducts\Block;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Collection\AbstractCollection;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\DataObject\IdentityInterface;

class ProductList extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Block\Product\ImageBuilder $_imageBuilder,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        \Magento\Catalog\Helper\Output $catalogHelper,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Magento\Catalog\Helper\Product\Compare $compareHelper,
        \Magento\Wishlist\Helper\Data $wishlistHelperData,
        array $data = []
    ) {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_productRepository = $productRepository;
        $this->_imageBuilder=$_imageBuilder;
        $this->urlHelper = $urlHelper;
        $this->catalogHelper = $catalogHelper;
        $this->priceHelper = $priceHelper;
        $this->compareHelper = $compareHelper;
        $this->wishlistHelperData = $wishlistHelperData;
        parent::__construct($context, $data);
    }

    /*
    * Get product collection
    */
    public function getProductCollection()
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        return $collection;
    }

    /*
    * Get product by sku
    */
    public function getProductBySku($sku)
    {
        return $this->_productRepository->get($sku);
    }
    
    public function getImage($product, $imageId, $attributes = [])
    {
        return $this->_imageBuilder->setProduct($product)
                ->setImageId($imageId)
                ->setAttributes($attributes)
                ->create();
    }

    public function getAddToCartPostParams(\Magento\Catalog\Model\Product $product)
    {
        $url = $this->getAddToCartUrl($product);
        return [
            'action' => $url,
            'data' => [
                'product' => $product->getEntityId(),
                \Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED =>
                    $this->urlHelper->getEncodedUrl($url),
            ]
        ];
    }

    /**
     * Get price helper.
     *
     * @return \Magento\Framework\Pricing\Helper\Data
     */
    public function getPriceHelper()
    {
        return $this->priceHelper;
    }

    /**
     * Get catalog helper.
     * @return \Magento\Catalog\Helper\Output
     */
    public function getCatalogHelper()
    {
        return $this->catalogHelper;
    }

    /**
     * Get catalog helper.
     *
     * @return  \Magento\Catalog\Helper\Product\Compare
     */
    public function getCompareHelper()
    {
        return $this->compareHelper;
    }

    /**
     * Get catalog helper.
     *
     * @return \Magento\Wishlist\Helper\Data
     */
    public function getWishlistHelperData()
    {
        return $this->wishlistHelperData;
    }
}
