<?php
/**
 * Techies India Inc.
 * This plugin use for FlatRate Shipping Modify rates
 */
namespace Techies\Flatrate\Plugin;

use Magento\OfflineShipping\Model\Carrier\Flatrate as Subject;
use Techies\Flatrate\Model\Flatrate;
use Techies\Flatrate\Helper\Data;
use Psr\Log\LoggerInterface;
use Magento\Shipping\Model\Rate\Result;
use Magento\Quote\Model\Quote\Address\RateRequest;

class OfflineFlatRatePlugin
{
    /**
     * @var \Techies\Flatrate\Helper\Data
     */
    private $helper;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    /**
     * @var \Techies\Flatrate\Model\Flatrate
     */
    private $flatrate;
    public function __construct(Flatrate $flatrate, Data $helper, LoggerInterface $logger)
    {
        $this->helper = $helper;
        $this->logger = $logger;
        $this->flatrate = $flatrate;
    }
    // in-progress depend on cart functionality
    public function afterCollectRates(Subject $subject, Result $results, RateRequest $rateRequest)
    {
        try {
            /*
            * IsEnbale flatrate custom funtionality
            */
            $isEnable = $this->helper->isEnabled();
            if ($isEnable) {
                $shippingPrice = $this->flatrate->getProductPriceByQty($rateRequest); // get product shipping price
                if (!empty($shippingPrice)) {
                    $shippingRates = $results->getAllRates();
                    foreach ($shippingRates as $rate) { // set custom rate for flat rate
                        $rate->setCost($shippingPrice);
                        $rate->setPrice($shippingPrice);
                    }
                }
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
        return $results;
    }
}
