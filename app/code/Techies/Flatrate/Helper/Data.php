<?php
/*
 * Techies India Inc.
 */

namespace Techies\Flatrate\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Json\Helper\Data as JsonHelperData;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    private $jsonHelper;

    /**
     *  ScopeConfigInterface
     * @var $scopeConfig
     */

    public function __construct(ScopeConfigInterface $scopeConfig, JsonHelperData $jsonHelper)
    {
        $this->scopeConfig = $scopeConfig;
        $this->jsonHelper = $jsonHelper;
    }

    /**
     * checking function is Enabled
     * @return boolean
     */
    public function isEnabled()
    {

        return (bool)$this->scopeConfig->getValue(
            'techies_flatrate/flatrate/flatrate_active',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /*
     * retun price value , if sample product buy from buynow tab on product detail page
     */
    public function sampleProductBuyNowPrice()
    {

        return $this->scopeConfig->getValue(
            'techies_flatrate/flatrate/sample_product_buynow_tab_price',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return price, First sapmle product price value;
     */
    public function sampleProductFirstPrice()
    {

        return $this->scopeConfig->getValue(
            'techies_flatrate/flatrate/first_price',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /*
     * return second sample product price value;
     */
    public function sampleProductSecondPrice()
    {

        return $this->scopeConfig->getValue(
            'techies_flatrate/flatrate/second_product_shipping',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /*
     * return max sample product qty, applicable depend on max sample product qty
     */
    public function sampleProductMaxQty()
    {

        return $this->scopeConfig->getValue(
            'techies_flatrate/flatrate/sample_max_qty',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /*
     * return max bulk sample product qty;
     */
    public function sampleProductBulkMaxQty()
    {

        return $this->scopeConfig->getValue(
            'techies_flatrate/flatrate/sample_bulk_max_qty',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /*
     * return flatRate shipping custom price;
     */
    public function flatrateCustomPrice()
    {

        return $this->scopeConfig->getValue(
            'techies_flatrate/flatrate/flatrate_custom_price',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /*
     * return flatrate shipping custom handling fee
     */
    public function flatrateCustomHandlingPrice()
    {

        return $this->scopeConfig->getValue(
            'techies_flatrate/flatrate/flatrate_custom_handling_price',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /*
     * return price depend on qty range;
     * @param qty
     */
    public function flatrateRangesPriceByQty($qty)
    {
        $priceRange = $this->scopeConfig->getValue(
            'techies_flatrate/flatrate/flatrate_ranges',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if (isset($priceRange) && !empty($priceRange)) {
            $priceArray = $this->jsonHelper->jsonDecode($priceRange);
            if (is_array($priceArray) && count($priceArray) > 0) {
                foreach ($priceArray as $key => $value) {
                    $from_qty = $value['from_qty'];
                    $to_qty = $value['to_qty'];
                    $price = $value['price'];
                    //$qty >= 100 && 250 > $qty
                    if ($qty >= $from_qty && $to_qty > $qty) {
                        return $price;
                    } elseif ($qty >= $from_qty && $to_qty == $from_qty) {
                        if ($qty > $from_qty) {
                            return $this->flatrateCustomPrice() + $this->flatrateCustomHandlingPrice();
                        } else {
                            return $price;
                        }
                    }
                }
            }
        }
    }

    /*
    * return max qty, into flatrate_ranges qty;
    *
    */
    public function flatrateRangesMaxQty()
    {
        $priceRange = $this->scopeConfig->getValue(
            'techies_flatrate/flatrate/flatrate_ranges',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $max[] = '';
        if (isset($priceRange) && !empty($priceRange)) {
            $priceArray = $this->jsonHelper->jsonDecode($priceRange);
            if (is_array($priceArray) && count($priceArray) > 0) {
                foreach ($priceArray as $key => $value) {
                    $from_qty = $value['from_qty'];
                    $max[$from_qty] = $from_qty;
                }
            }
            return max($max);
        }
    }
}
