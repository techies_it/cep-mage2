<?php
/**
 * Techies India Inc.
 */

namespace Techies\Flatrate\Model;

use Techies\Flatrate\Helper\Data;
use Magento\Framework\Json\Helper\Data as MagentoHelperData;
use Magento\Checkout\Model\Cart;

class Flatrate extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @var \Techies\Flatrate\Helper\Data
     */
    private $helper;
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    private $jsonHelper;

    /*
     *  @var \Magento\Checkout\Model\Cart
     */
    private $cart;

    public function __construct(Data $helper, MagentoHelperData $jsonHelper, Cart $cart)
    {
        $this->helper = $helper;
        $this->jsonHelper = $jsonHelper;
        $this->cart = $cart;
    }

    /*
     * return price depend on qty range;
     */
    public function getProductPriceByQty($request)
    {
        $price = 0;
        /*
         * get sample max qty
         */
        $maxQty = $this->helper->sampleProductMaxQty();
        /*
         * get from config is enable;
         */

        $qty = $request->getPackageQty();
        if ($request->getAllItems()) {
            if ($qty > $maxQty) {
                //get price range from config serialize value;
                $shippinPrice = $this->helper->flatrateRangesPriceByQty($qty);
                if (isset($shippinPrice) && !empty($shippinPrice)) {
                    $price = ($qty * $shippinPrice);
                }
            } else {
                if ($qty <= $maxQty) {
                    // geting sample product price
                    $price = $this->getRequestSampleProductPrice();
                }
            }
        }
        return $price;
    }

    /*
     * return sample product price from request
     */
    public function getRequestSampleProductPrice()
    {
        $qty = 0;

        $priceShipping = 0;
        /*
         * get sample max qty
         */
        $maxQty = $this->helper->sampleProductMaxQty();

        /*
        * sample 1 product price
        */
        $firstProductShipping = $this->helper->sampleProductFirstPrice();

        /*
         * sample second product price
         */
        $secondProductShipping = $this->helper->sampleProductSecondPrice();
        /*
         * get cat items from cart model
         */
        $cartItems = $this->cart->getQuote()->getAllItems();

        $price = 0;
        if ($cartItems) {
            foreach ($cartItems as $item) {
                $priceShipping = 0;
                $isSample = $item->getIsSample();  //$isSample=1 for sample tab, $isSample=0 for buynow tab
                if (isset($isSample) && empty($isSample)) {
                    /*
                     * get sample product price from config
                     */
                    $firstProductShipping = $this->helper->sampleProductBuyNowPrice();
                    $secondProductShipping = $this->helper->sampleProductBuyNowPrice();
                }
                $qty = $item->getQty();
                if ($qty <= $maxQty) {
                    if ($qty == 1) {
                        $priceShipping += $firstProductShipping;
                    } else {
                        $priceShipping += $firstProductShipping;
                        $priceShipping += ($qty - 1) * $secondProductShipping;
                    }
                    $price += $priceShipping;
                }
            }
        }

        return $price;
    }
}
