<?php
/**
 * Techies India Inc. 2020
 */

namespace Techies\RequestQuotePayment\Api\Data;

/**
 * @api
 */
interface PaymentInterface
{
    const PAYMENT_ID = 'payment_id';
    const CC_OWNER = 'cc_owner';
    const BILLING_STREET_ADDRESS = 'billing_street_address';
    const BILLING_CITY = 'billing_city';
    const BILLING_STATE = 'billing_state';
    const BILLING_ZIPCODE = 'billing_zipcode';
    const INVOICE_NUMBER = 'invoice_number';
    const CC_AMOUNT = 'cc_amount';
    const SUBSCRIBE = 'subscribe';
    const API_RSSPONSE = 'api_response';
    const API_STATUS = 'api_status';
    const SHORT_NAME = 'short_name';
    /**
     * @var string
     */
    const STORE_ID = 'store_id';
    /**
     * @var string
     */
    const META_TITLE = 'meta_title';
    /**
     * @var string
     */
    const META_DESCRIPTION = 'meta_description';
    /**
     * @var string
     */
    const META_KEYWORDS = 'meta_keywords';
    /**
     * @var string
     */
    const IS_ACTIVE = 'is_active';
    /**
     * @var int
     */
    const STATUS_ENABLED = 1;
    /**
     * @var int
     */
    const STATUS_DISABLED = 2;

    /**
     * @param int $id
     * @return PaymentInterface
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return PaymentInterface
     */
    public function setPaymentId($id);

    /**
     * @return int
     */
    public function getPaymentId();

    /**
     * @param string $ccOwner
     * @return PaymentInterface
     */
    public function setCcOwner($ccOwner);

    /**
     * @return string
     */
    public function getCcOwner();

    /**
     * @param string $billingStreetAddress
     * @return PaymentInterface
     */
    public function setBillingStreetAddress($billingStreetAddress);

    /**
     * @return string
     */
    public function getBillingStreetAddress();

    /**
     * @param string $billingCity
     * @return PaymentInterface
     */
    public function setBillingCity($billingCity);

    /**
     * @return string
     */
    public function getBillingCity();

    /**
     * @param int $billingState
     * @return PaymentInterface
     */
    public function setBillingState($billingState);

    /**
     * @return int
     */
    public function getBillingState();

    /**
     * @param string $billingZipcode
     * @return PaymentInterface
     */
    public function setBillingZipcode($billingZipcode);

    /**
     * @return string
     */
    public function getBillingZipcode();

    /**
     * @param string $invoiceNumber
     * @return PaymentInterface
     */
    public function setInvoiceNumber($invoiceNumber);

    /**
     * @return string
     */
    public function getInvoiceNumber();

    /**
     * @param float $ccAmount
     * @return PaymentInterface
     */
    public function setCcAmount($ccAmount);

    /**
     * @return float
     */
    public function getCcAmount();

    /**
     * @param int $subscribe
     * @return PaymentInterface
     */
    public function setSubscribe($subscribe);

    /**
     * @return int
     */
    public function getSubscribe();

    /**
     * @param int[] $store
     * @return PaymentInterface
     */
    public function setStoreId(array $store);

    /**
     * @return int[]
     */
    public function getStoreId();

    /**
     * @param string $metaTitle
     * @return PaymentInterface
     */
    public function setMetaTitle($metaTitle);

    /**
     * @return string
     */
    public function getMetaTitle();

    /**
     * @param string $metaDescription
     * @return PaymentInterface
     */
    public function setMetaDescription($metaDescription);

    /**
     * @return string
     */
    public function getMetaDescription();

    /**
     * @param string $metaKeywords
     * @return PaymentInterface
     */
    public function setMetaKeywords($metaKeywords);

    /**
     * @return string
     */
    public function getMetaKeywords();

    /**
     * @param int $isActive
     * @return PaymentInterface
     */
    public function setIsActive($isActive);

    /**
     * @return int
     */
    public function getIsActive();

    public function setApiResponse($api_response);

    public function getApiResponse();

    public function setApiStatus($api_status);

    public function getApiStatus();

    public function setShortName($short_name);

    public function getShortName();
}
