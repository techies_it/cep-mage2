<?php
/**
 * Techies India Inc. 2020
 */

namespace Techies\RequestQuotePayment\Api\Data;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * @api
 */
interface PaymentSearchResultInterface
{
    /**
     * get items
     *
     * @return \Techies\RequestQuotePayment\Api\Data\PaymentInterface[]
     */
    public function getItems();

    /**
     * Set items
     *
     * @param \Techies\RequestQuotePayment\Api\Data\PaymentInterface[] $items
     * @return $this
     */
    public function setItems(array $items);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return $this
     */
    public function setSearchCriteria(SearchCriteriaInterface $searchCriteria);

    /**
     * @param int $count
     * @return $this
     */
    public function setTotalCount($count);
}
