<?php
/**
 * Techies India Inc. 2020
 */

namespace Techies\RequestQuotePayment\Api;

use Techies\RequestQuotePayment\Api\Data\PaymentInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * @api
 */
interface PaymentRepositoryInterface
{
    /**
     * @param PaymentInterface $Payment
     * @return PaymentInterface
     */
    public function save(PaymentInterface $Payment);

    /**
     * @param $id
     * @return PaymentInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($id);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Techies\RequestQuotePayment\Api\Data\PaymentSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param PaymentInterface $Payment
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(PaymentInterface $Payment);

    /**
     * @param int $PaymentId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($PaymentId);

    /**
     * clear caches instances
     * @return void
     */
    public function clear();
}
