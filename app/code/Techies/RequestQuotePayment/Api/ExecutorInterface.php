<?php
/**
 * Techies India Inc. 2020
 */

namespace Techies\RequestQuotePayment\Api;

/**
 * @api
 */
interface ExecutorInterface
{
    /**
     * execute
     * @param int $id
     */
    public function execute($id);
}
