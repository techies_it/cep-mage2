<?php
/**
 * Techies India Inc. 2020
 */

namespace Techies\RequestQuotePayment\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Payment extends AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('techies_request_quote_payment_payment', 'payment_id');
    }

    /**
     * Get store ids to which specified item is assigned
     *
     * @param int $paymentId
     * @return array
     */
    public function lookupStoreIds($paymentId)
    {
        $connection = $this->getConnection();

        $select = $connection->select()
            ->from(['store_table' => $this->getTable('techies_request_quote_payment_payment_store')], 'store_id')
            ->join(
                ['main_table' => $this->getMainTable()],
                'store_table.payment_id = main_table.payment_id',
                []
            )->where('main_table.payment_id = :payment_id');
        return $connection->fetchCol($select, ['payment_id' => (int)$paymentId]);
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel | \Techies\RequestQuotePayment\Model\Payment $object
     * @return $this | AbstractDb
     */
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        $oldStores = $this->lookupStoreIds($object->getId());
        $newStores = (array)$object->getStoreId();
        $table  = $this->getTable('techies_request_quote_payment_payment_store');
        $insert = array_diff($newStores, $oldStores);
        $delete = array_diff($oldStores, $newStores);
        if ($delete) {
            $where = [
                'payment_id = ?' => (int) $object->getId(),
                'store_id IN (?)' => $delete
            ];
            $this->getConnection()->delete($table, $where);
        }

        if ($insert) {
            $data = [];
            foreach ($insert as $storeId) {
                $data[] = [
                    'payment_id'  => (int) $object->getId(),
                    'store_id' => (int) $storeId
                ];
            }
            $this->getConnection()->insertMultiple($table, $data);
        }
        return parent::_afterSave($object);
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel | \Techies\RequestQuotePayment\Model\Payment $object
     * @return $this|AbstractModel
     */
    protected function _afterLoad(\Magento\Framework\Model\AbstractModel $object)
    {
        parent::_afterLoad($object);
        $object->setStoreId($this->lookupStoreIds($object->getId()));
        return $this;
    }
}
