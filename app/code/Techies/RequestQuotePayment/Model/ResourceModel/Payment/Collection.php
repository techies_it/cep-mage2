<?php
/**
 * Techies India Inc. 2020
 */

namespace Techies\RequestQuotePayment\Model\ResourceModel\Payment;

use Techies\RequestQuotePayment\Model\Payment;
use Techies\RequestQuotePayment\Model\ResourceModel\AbstractCollection;

/**
 * @api
 */
class Collection extends AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Payment::class,
            \Techies\RequestQuotePayment\Model\ResourceModel\Payment::class
        );
        $this->_map['fields']['store_id'] = 'store_table.store_id';
        $this->_map['fields']['payment_id'] = 'main_table.payment_id';
    }

    /**
     * after collection load
     */
    protected function _afterLoad()
    {
        $ids = [];
        foreach ($this as $item) {
            $ids[] = $item->getId();
        }
        if (count($ids)) {
            $connection = $this->getConnection();
            $select = $connection->select()->from(
                ['store_table' => $this->getTable('techies_request_quote_payment_payment_store')]
            )->where('store_table.payment_id IN (?)', $ids);
            $result = $connection->fetchAll($select);
            if ($result) {
                $storesData = [];
                foreach ($result as $storeData) {
                    $storesData[$storeData['payment_id']][] = $storeData['store_id'];
                }
                foreach ($this as $item) {
                    $linkedId = $item->getData('payment_id');
                    if (!isset($storesData[$linkedId])) {
                        continue;
                    }
                    $item->setData('store_id', $storesData[$linkedId]);
                }
            }
        }
        return parent::_afterLoad();
    }

    /**
     * @param $storeId
     * @return $this
     */
    public function addStoreFilter($storeId)
    {
        if (!isset($this->joinFields['store'])) {
            $this->getSelect()->joinLeft(
                [
                    'related_store' => $this->getTable('techies_request_quote_payment_payment_store')
                ],
                'related_store.payment_id = main_table.payment_id'
            );
            $this->getSelect()->where('related_store.store_id IN (?)', [$storeId, 0]);
            $this->joinFields['store'] = true;
        }
        return $this;
    }

    /**
     * Join store relation table
     *
     * @return void
     */
    protected function _renderFiltersBefore()
    {
        $this->getSelect()->joinLeft(
            ['store_table' => $this->getTable('techies_request_quote_payment_payment_store')],
            'main_table.payment_id = store_table.payment_id',
            []
        )->group(
            'main_table.payment_id'
        );
        parent::_renderFiltersBefore();
    }
}
