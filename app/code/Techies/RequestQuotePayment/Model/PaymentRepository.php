<?php
/**
 * Techies India Inc. 2020
 */

namespace Techies\RequestQuotePayment\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\ValidatorException;
use Techies\RequestQuotePayment\Api\Data\PaymentInterface;
use Techies\RequestQuotePayment\Api\Data\PaymentInterfaceFactory;
use Techies\RequestQuotePayment\Api\Data\PaymentSearchResultInterfaceFactory;
use Techies\RequestQuotePayment\Api\PaymentRepositoryInterface;
use Techies\RequestQuotePayment\Model\ResourceModel\Payment as PaymentResourceModel;
use Techies\RequestQuotePayment\Model\ResourceModel\Payment\Collection;
use Techies\RequestQuotePayment\Model\ResourceModel\Payment\CollectionFactory as PaymentCollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Techies\RequestQuotePayment\Helper\Data;

class PaymentRepository implements PaymentRepositoryInterface
{
    /**
     * Cached instances
     *
     * @var array
     */
    protected $instances = [];

    /**
     * Payment authorization resource model
     *
     * @var PaymentResourceModel
     */
    protected $resource;

    /**
     * Payment authorization collection factory
     *
     * @var PaymentCollectionFactory
     */
    protected $paymentCollectionFactory;

    /**
     * Payment authorization interface factory
     *
     * @var PaymentInterfaceFactory
     */
    protected $paymentInterfaceFactory;

    /**
     * Data Object Helper
     *
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * Search result factory
     *
     * @var PaymentSearchResultInterfaceFactory
     */
    protected $searchResultsFactory;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    protected $helperData;

    /*
     * \Magento\Framework\HTTP\Client\Curl $curl
     */
    protected $curl;
    /*
     * \Psr\Log\LoggerInterface $curl
     */
    protected $loggerInterface;

    /*
     * SerializerInterface $serializerInterface
     */
    protected $serializerInterface;

    /**
     * constructor
     * @param PaymentResourceModel $resource
     * @param PaymentCollectionFactory $paymentCollectionFactory
     * @param PaymentnterfaceFactory $paymentInterfaceFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param PaymentSearchResultInterfaceFactory $searchResultsFactory
     * @param \Magento\Framework\HTTP\Client\Curl $curl
     * @param \Psr\Log\LoggerInterface $loggerInterface
     *
     */

    public function __construct(
        PaymentResourceModel $resource,
        PaymentCollectionFactory $paymentCollectionFactory,
        PaymentInterfaceFactory $paymentInterfaceFactory,
        DataObjectHelper $dataObjectHelper,
        PaymentSearchResultInterfaceFactory $searchResultsFactory,
        ScopeConfigInterface $scopeConfig,
        Data $helperData,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Psr\Log\LoggerInterface $loggerInterface,
        SerializerInterface $serializerInterface
    ) {

        $this->resource = $resource;
        $this->paymentCollectionFactory = $paymentCollectionFactory;
        $this->paymentInterfaceFactory = $paymentInterfaceFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->scopeConfig = $scopeConfig;
        $this->helperData = $helperData;
        $this->curl = $curl;
        $this->log = $loggerInterface;
        $this->serializerInterface=$serializerInterface;
    }

    /**
     * Save Payment authorization.
     *
     * @param \Techies\RequestQuotePayment\Api\Data\PaymentInterface $payment
     * @return \Techies\RequestQuotePayment\Api\Data\PaymentInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(PaymentInterface $payment)
    {
        /** @var PaymentInterface|\Magento\Framework\Model\AbstractModel $payment */
        try {
            $this->resource->save($payment);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the Payment authorization: %1',
                $exception->getMessage()
            ));
        }
        return $payment;
    }

    /**
     * Retrieve Payment authorization
     *
     * @param int $paymentId
     * @return \Techies\RequestQuotePayment\Api\Data\PaymentInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($paymentId)
    {
        if (!isset($this->instances[$paymentId])) {
            /** @var PaymentInterface|\Magento\Framework\Model\AbstractModel $payment */
            $payment = $this->paymentInterfaceFactory->create();
            $this->resource->load($payment, $paymentId);
            if (!$payment->getId()) {
                throw new NoSuchEntityException(__('Requested Payment authorization doesn\'t exist'));
            }
            $this->instances[$paymentId] = $payment;
        }
        return $this->instances[$paymentId];
    }

    /**
     * Retrieve Payment Authorizations matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Techies\RequestQuotePayment\Api\Data\PaymentSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Techies\RequestQuotePayment\Api\Data\PaymentSearchResultInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Techies\RequestQuotePayment\Model\ResourceModel\Payment\Collection $collection */
        $collection = $this->paymentCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var \Magento\Framework\Api\Search\FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? SortOrder::SORT_ASC : SortOrder::SORT_DESC
                );
            }
        } else {
            $collection->addOrder('main_table.' . PaymentInterface::PAYMENT_ID, SortOrder::SORT_ASC);
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var PaymentInterface[] $payments */
        $payments = [];
        /** @var \Techies\RequestQuotePayment\Model\Payment $payment */
        foreach ($collection as $payment) {
            /** @var PaymentInterface $paymentDataObject */
            $paymentDataObject = $this->paymentInterfaceFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $paymentDataObject,
                $payment->getData(),
                PaymentInterface::class
            );
            $payments[] = $paymentDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($payments);
    }

    /**
     * Delete Payment authorization
     *
     * @param PaymentInterface $payment
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(PaymentInterface $payment)
    {
        /** @var PaymentInterface|\Magento\Framework\Model\AbstractModel $payment */
        $id = $payment->getId();
        try {
            unset($this->instances[$id]);
            $this->resource->delete($payment);
        } catch (ValidatorException $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new StateException(
                __('Unable to removePayment authorization %1', $id)
            );
        }
        unset($this->instances[$id]);
        return true;
    }

    /**
     * Delete Payment authorization by ID.
     *
     * @param int $paymentId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($paymentId)
    {
        $payment = $this->get($paymentId);
        return $this->delete($payment);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param FilterGroup $filterGroup
     * @param Collection $collection
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function addFilterGroupToCollection(FilterGroup $filterGroup, Collection $collection)
    {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }

    /**
     * clear caches instances
     * @return void
     */
    public function clear()
    {
        $this->instances = [];
    }

    /*
     * capture payment payeezy
     * @param $dataForm
     */
    public function secureCheckoutPayment($dataForm)
    {
        $isEnabledQuotePayment = $this->helperData->isEnabledQuotePayment();
        if ($isEnabledQuotePayment) {
            $type = 'live';
            $short_name = (!isset($dataForm['short_name'])) ? 'ce' : $dataForm['short_name'];
            $emails = $this->helperData->quotePaymentEmails($short_name);
            $isTestMode = $this->helperData->isTestMode();
            if ($isTestMode) {
                $type = 'sandbox';
            }

            /*
             * geting config data ;
             */
            $serviceURL = $this->helperData->quotePaymentApiUrl($type);
            $apiKey = $this->helperData->quotePaymentApiKey($type);
            $apiSecret = $this->helperData->quotePaymentApiSecret($type);
            $token = $this->helperData->quotePaymentApiToken($type);

            $nonce = (string)hexdec(bin2hex(openssl_random_pseudo_bytes(4)));
            $timestamp = (string)time() * 1000; /* time stamp in milli seconds */
            $payload = $this->getPayload($dataForm);
            $data = $apiKey . $nonce . $timestamp . $token . $payload;
            $hashAlgorithm = "sha256";

            /*
             * Make sure the HMAC hash is in hex
             */
            $hmac = hash_hmac($hashAlgorithm, $data, $apiSecret, false);

            /*
             * Authorization : base64 of hmac hash
             */
            $hmac_enc = base64_encode($hmac);

            /**
             *  api headers;
             */
            $headers = [
                'Content-Type' => 'application/json',
                'apikey' =>$apiKey,
                'token' => $token,
                'Authorization' => $hmac_enc,
                'nonce' => $nonce,
                'timestamp' > $timestamp,
            ];

            /*
            * calling callingApiCurl
            */

            $callingApiCurl = $this->callingApiCurl($serviceURL, $headers, $payload);
            return $callingApiCurl;
        } else {
            return false;
        }
    }

    /*
     * calling callingApiCurl
     * @param payeezy curl $serviceURL= request url, curl $headers , $payload form data
     * return array
     */
    public function callingApiCurl($serviceURL, $headers, $payload)
    {
        $result = [];

        try {
            $this->curl->setHeaders($headers);
            $this->curl->post(
                $serviceURL,
                $payload
            );
            $result = $this->curl->getBody();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
        $response = json_decode($result, true);
        $description = [];
        $array['message'] = '';
        $array['api_status'] = 0;
        $array['api_status'] = '';
        if (count($response) > 0) {
            /*
             * set response
             */
            $array = $this->payeezyResponse($response);
        }
        return $array;
    }

    /*
     * set array for curl call
     * return json_encode values
     */
    public function getPayload($args = [])
    {
        $data = "";
        $amount = 0;
        $amt = (str_replace(["$", ","], '', $args['cc_amount']) * 100);
        $cc_exp_year=substr($this->processInput($args['cc_exp_year']), -2);
        $data = [
            'merchant_ref' => "CEP CC PAGE",
            'transaction_type' => "purchase",
            'method' => 'credit_card',
            'amount' => (int)$amt,
            'currency_code' => "USD",
            "partial_redemption" => "false",
            'credit_card' => [
                'type' => $this->processInput($this->helperData->getCardType($args['cc_type'])),
                'cardholder_name' => $this->processInput($args['cc_owner']),
                'card_number' => $this->processInput($args['cc_number']),
                'exp_date' => $this->processInput($args['cc_exp_month']) .$cc_exp_year ,
                'cvv' => $this->processInput($args['cc_cid']),
            ],
            "billing_address" => [
                "city" => $this->processInput($args['billing_city']),
                "street" => $this->processInput($args['billing_street_address']),
                "state_province" => $this->processInput($args['billing_state']),
                "zip_postal_code" => $this->processInput($args['billing_zipcode']),
            ],
            "level2" => [
                "customer_ref" => $this->processInput($args['invoice_number'])
            ]
        ];

        return json_encode($data, JSON_FORCE_OBJECT);
    }

    /*
     * remove stripslashes and extra space
     *return string
     */
    public function processInput($data)
    {

        $trim = trim($data);
        return (string)$trim;
    }

    /*
     * @param set response messages
     * return array
     */
    public function payeezyResponse($response)
    {
        $array['api_response'] = $this->serializerInterface->serialize($response);
        if (isset($response['Error']) && count($response['Error']) > 0) {
            if (isset($response['Error']['messages']) && count($response['Error']['messages']) > 0) {
                $messages = $response['Error']['messages'];
                if (isset($messages) && count($messages) > 0) {
                    foreach ($messages as $k => $val) {
                        $description[] = $val['description'];
                    }
                    $array['message'] = implode("<br>", $description);
                }
            }
        } elseif (isset($response['fault']) && count($response['fault']) > 0) {
            $messages = $response['fault']['detail'];
            if (isset($messages) && count($messages) > 0) {
                foreach ($messages as $k => $val) {
                    $description[] = $val['errorcode'];
                }
                $array['message'] = implode("<br>", $description);
            }
        } elseif (isset($response['error']) && !empty($response['error'])) {
            $array['message'] = $response['error'];
        } else {
            if (isset($response['transaction_status']) && $response['transaction_status'] == 'approved') {
                $array['message'] = 'Your payment has been processed successfully.';
                $array['api_status'] = 1;
            } else {
                $array['message'] = $response['bank_message'];
            }
        }
        return $array;
    }
}
