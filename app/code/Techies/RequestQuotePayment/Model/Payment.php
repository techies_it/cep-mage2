<?php
/**
 * Techies India Inc. 2020
 */

namespace Techies\RequestQuotePayment\Model;

use Techies\RequestQuotePayment\Api\Data\PaymentInterface;
use Magento\Framework\Model\AbstractModel;
use Techies\RequestQuotePayment\Model\ResourceModel\Payment as PaymentResourceModel;
use Magento\Framework\Option\ArrayInterface;

/**
 * @method \Techies\RequestQuotePayment\Model\ResourceModel\Page _getResource()
 * @method \Techies\RequestQuotePayment\Model\ResourceModel\Page getResource()
 */
class Payment extends AbstractModel implements PaymentInterface
{
    /**
     * this is use for Cache tag
     * In block xml file
     *
     * @var string
     */
    const CACHE_TAG = 'techies_requestquotepayment_payment';
    /**
     * Cache tag
     *
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;
    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'techies_requestquotepayment_payment';
    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'payment';
    /**
     * @var ArrayInterface[]
     */
    protected $optionProviders;

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(PaymentResourceModel::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get Page id
     *
     * @return array
     */
    public function getPaymentId()
    {
        return $this->getData(PaymentInterface::PAYMENT_ID);
    }

    /**
     * set payment authorization id
     *
     * @param int $paymentId
     * @return PaymentInterface
     */
    public function setPaymentId($paymentId)
    {
        return $this->setData(PaymentInterface::PAYMENT_ID, $paymentId);
    }

    /**
     * @param string $ccOwner
     * @return PaymentInterface
     */
    public function setCcOwner($ccOwner)
    {
        return $this->setData(PaymentInterface::CC_OWNER, $ccOwner);
    }

    /**
     * @return string
     */
    public function getCcOwner()
    {
        return $this->getData(PaymentInterface::CC_OWNER);
    }

    /**
     * @param string $billingStreetAddress
     * @return PaymentInterface
     */
    public function setBillingStreetAddress($billingStreetAddress)
    {
        return $this->setData(PaymentInterface::BILLING_STREET_ADDRESS, $billingStreetAddress);
    }

    /**
     * @return string
     */
    public function getBillingStreetAddress()
    {
        return $this->getData(PaymentInterface::BILLING_STREET_ADDRESS);
    }

    /**
     * @param string $billingCity
     * @return PaymentInterface
     */
    public function setBillingCity($billingCity)
    {
        return $this->setData(PaymentInterface::BILLING_CITY, $billingCity);
    }

    /**
     * @return string
     */
    public function getBillingCity()
    {
        return $this->getData(PaymentInterface::BILLING_CITY);
    }

    /**
     * @param int $billingState
     * @return PaymentInterface
     */
    public function setBillingState($billingState)
    {
        return $this->setData(PaymentInterface::BILLING_STATE, $billingState);
    }

    /**
     * @return int
     */
    public function getBillingState()
    {
        return $this->getData(PaymentInterface::BILLING_STATE);
    }

    /**
     * @param string $billingZipcode
     * @return PaymentInterface
     */
    public function setBillingZipcode($billingZipcode)
    {
        return $this->setData(PaymentInterface::BILLING_ZIPCODE, $billingZipcode);
    }

    /**
     * @return string
     */
    public function getBillingZipcode()
    {
        return $this->getData(PaymentInterface::BILLING_ZIPCODE);
    }

    /**
     * @param string $invoiceNumber
     * @return PaymentInterface
     */
    public function setInvoiceNumber($invoiceNumber)
    {
        return $this->setData(PaymentInterface::INVOICE_NUMBER, $invoiceNumber);
    }

    /**
     * @return string
     */
    public function getInvoiceNumber()
    {
        return $this->getData(PaymentInterface::INVOICE_NUMBER);
    }

    /**
     * @param float $ccAmount
     * @return PaymentInterface
     */
    public function setCcAmount($ccAmount)
    {
        return $this->setData(PaymentInterface::CC_AMOUNT, $ccAmount);
    }

    /**
     * @return float
     */
    public function getCcAmount()
    {
        return $this->getData(PaymentInterface::CC_AMOUNT);
    }

    /**
     * @param int $subscribe
     * @return PaymentInterface
     */
    public function setSubscribe($subscribe)
    {
        return $this->setData(PaymentInterface::SUBSCRIBE, $subscribe);
    }

    /**
     * @return int
     */
    public function getSubscribe()
    {
        return $this->getData(PaymentInterface::SUBSCRIBE);
    }

    /**
     * @param array $storeId
     * @return PaymentInterface
     */
    public function setStoreId(array $storeId)
    {
        return $this->setData(PaymentInterface::STORE_ID, $storeId);
    }

    /**
     * @return int[]
     */
    public function getStoreId()
    {
        return $this->getData(PaymentInterface::STORE_ID);
    }

    /**
     * @param string $metaTitle
     * @return PaymentInterface
     */
    public function setMetaTitle($metaTitle)
    {
        return $this->setData(PaymentInterface::META_TITLE, $metaTitle);
    }

    /**
     * @return string
     */
    public function getMetaTitle()
    {
        return $this->getData(PaymentInterface::META_TITLE);
    }

    /**
     * @param string $metaDescription
     * @return PaymentInterface
     */
    public function setMetaDescription($metaDescription)
    {
        return $this->setData(PaymentInterface::META_DESCRIPTION, $metaDescription);
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->getData(PaymentInterface::META_DESCRIPTION);
    }

    /**
     * @param string $metaKeywords
     * @return PaymentInterface
     */
    public function setMetaKeywords($metaKeywords)
    {
        return $this->setData(PaymentInterface::META_KEYWORDS, $metaKeywords);
    }

    /**
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->getData(PaymentInterface::META_KEYWORDS);
    }

    /**
     * @param int $isActive
     * @return PaymentInterface
     */
    public function setIsActive($isActive)
    {
        return $this->setData(PaymentInterface::IS_ACTIVE, $isActive);
    }

    /**
     * @return int
     */
    public function getIsActive()
    {
        return $this->getData(PaymentInterface::IS_ACTIVE);
    }

    /**
     * @param string $api_response
     * @return PaymentInterface
     */
    public function setApiResponse($api_response)
    {
        return $this->setData(PaymentInterface::API_RSSPONSE, $api_response);
    }

    /**
     * @return string
     */
    public function getApiResponse()
    {
        return $this->getData(PaymentInterface::API_RSSPONSE);
    }

    /**
     * @param string $api_status
     * @return PaymentInterface
     */
    public function setApiStatus($api_status)
    {
        return $this->setData(PaymentInterface::API_STATUS, $api_status);
    }

    /**
     * @return string
     */
    public function getApiStatus()
    {
        return $this->getData(PaymentInterface::API_STATUS);
    }

    /**
     * @param string $short_name
     * @return PaymentInterface
     */
    public function setShortName($short_name)
    {
        return $this->setData(PaymentInterface::SHORT_NAME, $short_name);
    }

    /**
     * @return string
     */
    public function getShortName()
    {
        return $this->getData(PaymentInterface::SHORT_NAME);
    }

    /**
     * @param $attribute
     * @return string
     */
    public function getAttributeText($attribute)
    {
        if (!isset($this->optionProviders[$attribute])) {
            return '';
        }
        if (!($this->optionProviders[$attribute] instanceof ArrayInterface)) {
            return '';
        }
        $value = $this->getData($attribute);
        if (!is_array($value)) {
            $value = explode(',', $value);
        }
        $keyValuePair = array_filter(
            $this->optionProviders[$attribute]->toOptionArray(),
            function ($item) use ($value) {
                return in_array($item['value'], $value);
            }
        );
        return implode(
            ', ',
            array_map(
                function ($item) {
                    return $item['label'];
                },
                $keyValuePair
            )
        );
    }
}
