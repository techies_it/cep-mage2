<?php
/**
 * Techies India Inc. 2020
 */

namespace Techies\RequestQuotePayment\Model\Payment\Executor;

use Techies\RequestQuotePayment\Api\PaymentRepositoryInterface;
use Techies\RequestQuotePayment\Api\ExecutorInterface;

class Delete implements ExecutorInterface
{
    /**
     * @var PaymentRepositoryInterface
     */
    private $paymentRepository;

    /**
     * Delete constructor.
     * @param PaymentRepositoryInterface $paymentRepository
     */
    public function __construct(
        PaymentRepositoryInterface $paymentRepository
    ) {
        $this->paymentRepository = $paymentRepository;
    }

    /**
     * @param int $id
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute($id)
    {
        $this->paymentRepository->deleteById($id);
    }
}
