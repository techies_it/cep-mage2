<?php
/**
 * Techies India Inc. 2020
 */


namespace Techies\RequestQuotePayment\Model\Payment;

use Magento\Framework\UrlInterface;
use Techies\RequestQuotePayment\Api\Data\PaymentInterface;

class Url
{
    /**
     * url builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;
    /**
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        UrlInterface $urlBuilder
    ) {
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * @param PaymentInterface $payment
     * @return string
     */
    public function getPaymentUrl(PaymentInterface $payment)
    {
        return $this->urlBuilder->getUrl('techies_request_quote_payment/payment/view', ['id' => $payment->getId()]);
    }
}
