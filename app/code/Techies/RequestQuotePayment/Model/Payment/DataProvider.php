<?php
/**
 * Techies India Inc. 2020
 */

namespace Techies\RequestQuotePayment\Model\Payment;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Techies\RequestQuotePayment\Model\ResourceModel\Payment\CollectionFactory as PaymentCollectionFactory;

class DataProvider extends AbstractDataProvider
{
    /**
     * Loaded data cache
     *
     * @var array
     */
    protected $loadedData;

    /**
     * Data persistor
     *
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param PaymentCollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        PaymentCollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var \Techies\RequestQuotePayment\Model\Payment $payment */
        foreach ($items as $payment) {
            $this->loadedData[$payment->getId()] = $payment->getData();
        }
        $data = $this->dataPersistor->get('techies_request_quote_payment_payment');
        if (!empty($data)) {
            $payment = $this->collection->getNewEmptyItem();
            $payment->setData($data);
            $this->loadedData[$payment->getId()] = $payment->getData();
            $this->dataPersistor->clear('techies_request_quote_payment_payment');
        }
        return $this->loadedData;
    }
}
