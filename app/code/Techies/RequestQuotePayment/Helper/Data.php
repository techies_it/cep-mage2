<?php
/**
 * Techies India Inc. 2020
 * this class use for config settings
 */

namespace Techies\RequestQuotePayment\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var SCOPE_STORE
     */
    const SCOPESTORE=\Magento\Store\Model\ScopeInterface::SCOPE_STORE;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /*
     * this function is checking isEnabledQuotePayment
     * return true/false
     */
    public function isEnabledQuotePayment()
    {
        return (bool)$this->scopeConfig->getValue(
            'techies_request_quote_payment/payment/quote_payment',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /*
     * this function is checking isTestMode
     * return true/false
     */
    public function isTestMode()
    {
        return (bool)$this->scopeConfig->getValue(
            'techies_request_quote_payment/payment/testmode',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /*
     * this function is checking  quotePaymentApiKey
     * @param $type
     * return api_key
     */
    public function quotePaymentApiKey($type = 'sandbox')
    {
        return $this->scopeConfig->getValue(
            'techies_request_quote_payment/payment/api_key_' . $type,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /*
     * this function is checking quotePaymentApiSecret
     * @param $type
     * return api_secret
     */
    public function quotePaymentApiSecret($type = 'sandbox')
    {
        return $this->scopeConfig->getValue(
            'techies_request_quote_payment/payment/api_secret_' . $type,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /*
    * this function is checking quotePaymentApiToken
    * @param $type
    * return api_token
    */
    public function quotePaymentApiToken($type = 'sandbox')
    {
        return $this->scopeConfig->getValue(
            'techies_request_quote_payment/payment/api_token_' . $type,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /*
       * this function is checking quotePaymentApiUrl
       * @param $type
       * return api_url
       */
    public function quotePaymentApiUrl($type = 'sandbox')
    {
        return $this->scopeConfig->getValue(
            'techies_request_quote_payment/payment/api_url_' . $type,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /*
       * this function is checking quotePaymentEmails
       * @param $ce
       * return email $emailArray

     */
    public function quotePaymentEmails($ce = 'ce')
    {
        $request='techies_request_quote_payment';
        $email = $this->scopeConfig->getValue($request.'/payment/email_for_' . $ce, self::SCOPESTORE);
        $emailCc = $this->scopeConfig->getValue($request.'/payment/email_for_cc_' . $ce, self::SCOPESTORE);
        $add_cc_email = $this->scopeConfig->getValue($request.'/payment/add_cc_email', self::SCOPESTORE);
        $emailArray = ['email' => $email, 'email_cc' => $emailCc, 'add_cc_email' => $add_cc_email];
        return $emailArray;
    }

    /*
       * this function is checking getCardType
       * @param $type
       * return $card type

     */
    public function getCardType($type)
    {
        if (!empty($type)) {
            if (strtolower($type) == 'vi') {
                $card = 'Visa';
            } elseif (strtolower($type) == 'ae') {
                $card = 'American Express';
            } elseif (strtolower($type) == 'mc') {
                $card = 'MasterCard';
            } elseif (strtolower($type) == 'di') {
                $card = 'Discover';
            }
        }
        return $card;
    }
}
