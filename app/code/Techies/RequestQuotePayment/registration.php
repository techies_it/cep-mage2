<?php
/**
 * Techies India Inc. 2020
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Techies_RequestQuotePayment',
    __DIR__
);
