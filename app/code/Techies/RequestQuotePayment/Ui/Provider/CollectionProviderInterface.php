<?php
namespace Techies\RequestQuotePayment\Ui\Provider;

interface CollectionProviderInterface
{
    /**
     * @return \Techies\RequestQuotePayment\Model\ResourceModel\AbstractCollection
     */
    public function getCollection();
}
