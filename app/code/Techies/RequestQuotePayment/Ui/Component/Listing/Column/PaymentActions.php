<?php
/**
 * Techies India Inc. 2020
 */

namespace Techies\RequestQuotePayment\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\Listing\Columns\Column;

class PaymentActions extends Column
{
    /**
     * Url path  to edit
     * @var string
     */
    const URL_PATH_EDIT = 'techies_requestquotepayment/payment/edit';

    /**
     * Url path  to delete
     * @var string
     */
    const URL_PATH_DELETE = 'techies_requestquotepayment/payment/delete';

    /**
     * Url builder
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * constructor
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['payment_id'])) {
                    $item[$this->getData('name')] = [
                        'edit' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_PATH_EDIT,
                                [
                                    'payment_id' => $item['payment_id']
                                ]
                            ),
                            'label' => __('Edit')
                        ],
                        'delete' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_PATH_DELETE,
                                [
                                    'payment_id' => $item['payment_id']
                                ]
                            ),
                            'label' => __('Delete'),
                            'confirm' => [
                                'title' => __('Delete "${ $.$data.cc_owner }"'),
                                'message' => __('Are you sure you want to delete')
                            ]
                        ]
                    ];
                }
            }
        }
        return $dataSource;
    }
}
