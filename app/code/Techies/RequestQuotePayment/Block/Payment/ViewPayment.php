<?php
/**
 * Techies India Inc. 2020
 */


namespace Techies\RequestQuotePayment\Block\Payment;

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Techies\RequestQuotePayment\Model\Payment\Source\BillingState;

/**
 * @api
 */
class ViewPayment extends Template
{
    /**
     * @var Registry
     */
    private $coreRegistry;
    protected $billingState;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param $imageBuilder
     * @param array $data
     */
    public function __construct(Context $context, Registry $registry, BillingState $billingState, array $data = [])
    {
        $this->billingState = $billingState;
        $this->coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * get current payment authorization
     *
     * @return \Techies\RequestQuotePayment\Api\Data\PaymentInterface
     */
    public function getCurrentPayment()
    {
        return $this->coreRegistry->registry('current_payment');
    }

    public function regions()
    {
        return $this->billingState->toOptionArray();
    }

    /**
     * Returns action url for contact form
     *
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('requestquotepayment/', ['_secure' => true]);
    }

    /**
     * Returns years
     *
     * @return array
     */
    public function getYear()
    {
        $year=[];
        $currentDate= date('Y');
        for ($y = $currentDate; $y <= $currentDate + 11; $y++) {
            $year[$y]=$y;
        }
        return $year;
    }
}
