<?php
/**
 * Techies India Inc. 2020
 */

namespace Techies\RequestQuotePayment\Block\Adminhtml\Button\Payment;

use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class Delete implements ButtonProviderInterface
{
    /**
     * @var Registry
     */
    private $registry;
    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * Delete constructor.
     * @param Registry $registry
     * @param UrlInterface $url
     */
    public function __construct(Registry $registry, UrlInterface $url)
    {
        $this->registry = $registry;
        $this->url = $url;
    }

    /**
     * get button data
     *
     * @return array
     */
    public function getButtonData()
    {
        $data = [];
        if ($this->getPaymentId()) {
            $data = [
                'label' => __('Delete payment authorization'),
                'class' => 'delete',
                'on_click' => 'deleteConfirm(\'' . __(
                    'Are you sure you want to do this?'
                ) . '\', \'' . $this->getDeleteUrl() . '\')',
                'sort_order' => 20,
            ];
        }
        return $data;
    }

    /**
     * @return \Techies\RequestQuotePayment\Api\Data\PaymentInterface | null
     */
    private function getPayment()
    {
        return $this->registry->registry('current_payment');
    }

    /**
     * @return int|null
     */
    private function getPaymentId()
    {
        $payment = $this->getPayment();
        return ($payment) ? $payment->getId() : null;
    }

    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        return $this->url->getUrl(
            '*/*/delete',
            [
                'payment_id' => $this->getpaymentId()
            ]
        );
    }
}
