<?php
/**
 * Techies India Inc. 2020
 */

namespace Techies\RequestQuotePayment\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Techies\RequestQuotePayment\Api\PaymentRepositoryInterface;
use Magento\Framework\Registry;
use Techies\RequestQuotePayment\Model\Payment\Url as UrlModel;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Api\DataObjectHelper;
use Techies\RequestQuotePayment\Api\Data\PaymentInterfaceFactory;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Psr\Log\LoggerInterface;
use Techies\RequestQuotePayment\Helper\Data;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;

class Index extends Action
{
    /**
     * @var string
     */
    const BREADCRUMBS_CONFIG_PATH = 'techies_request_quote_payment/payment/breadcrumbs';

    /**
     * @var string
     */
    const EMAIL = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

    /**
     * @var string
     */
    const EMAILNAME = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
    /**
     * @var \Techies\RequestQuotePayment\Api\PaymentRepositoryInterface
     */
    protected $paymentRepository;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var \Techies\RequestQuotePayment\Model\Payment\Url
     */
    protected $urlModel;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    protected $messageManager;
    /**
     * Data Object Helper
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;
    /**
     * Payment authorization factory
     * @var PaymentInterfaceFactory
     */
    protected $paymentFactory;
    /**
     * Payment $inlineTranslation
     * @var StateInterface
     */
    protected $inlineTranslation;
    /**
     * Payment $transportBuilder
     * @var TransportBuilder
     */
    protected $transportBuilder;
    /**
     * Payment $logLoggerInterface
     * @var LoggerInterface
     */
    protected $logLoggerInterface;
    /**
     * Payment $helperData
     * @var Data
     */
    protected $helperData;
    /**
     * Payment $remoteAddress
     * @var RemoteAddress
     */
    protected $remoteAddress;

    /**
     * @param Context $context
     * @param PaymentRepositoryInterface $paymentRepository
     * @param Registry $coreRegistry
     * @param UrlModel $urlModel
     * @param StoreManagerInterface $storeManager
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Context $context,
        PaymentRepositoryInterface $paymentRepository,
        Registry $coreRegistry,
        UrlModel $urlModel,
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig,
        ManagerInterface $messageManager,
        DataObjectHelper $dataObjectHelper,
        PaymentInterfaceFactory $paymentFactory,
        StateInterface $inlineTranslation,
        TransportBuilder $transportBuilder,
        LoggerInterface $loggerInterface,
        Data $helperData,
        RemoteAddress $remoteAddress
    ) {

        $this->paymentRepository = $paymentRepository;

        $this->coreRegistry = $coreRegistry;

        $this->urlModel = $urlModel;

        $this->storeManager = $storeManager;

        $this->scopeConfig = $scopeConfig;

        $this->messageManager = $messageManager;

        $this->dataObjectHelper = $dataObjectHelper;

        $this->paymentFactory = $paymentFactory;

        $this->inlineTranslation = $inlineTranslation;

        $this->transportBuilder = $transportBuilder;

        $this->logLoggerInterface = $loggerInterface;

        $this->helperData = $helperData;

        $this->remoteAddress = $remoteAddress;

        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\Result\Forward|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $url = 'cc-authorization';
        try {
            $params = $this->getRequest()->getParams();
            $msg = '';
            if (isset($params) && count($params) > 0) {
                $url = 'cc-authorization-' . $params['short_name'];
                unset($params['form_key']);
                unset($params['hideit']);
                /*
                 * @process payment Api Techies\RequestQuotePayment\Model\Payment
                 * @var form post params
                 */
                if (!empty($params)) {
                    /** @var PaymentInterface $payment */
                    $payment = $this->paymentRepository->secureCheckoutPayment($params);
                    if (isset($payment) && count($payment) > 0) {
                        $msg = $payment['message'];
                        if (isset($payment['api_status']) && $payment['api_status'] == 1) {
                            // Send Mail
                            $this->sendEmail($params, $msg);

                            $this->messageManager->addSuccess(__($msg));
                        } else {
                            $this->messageManager->addError(__($msg));
                        }
                        /**
                         * save user details only
                         */
                        $paymentData = $this->paymentFactory->create();
                        $params['api_status'] = $payment['api_status'];
                        $params['api_response'] = $payment['api_response'];
                        $params['short_name'] = $params['short_name'];
                        $params['subscribe'] = 1;
                        unset($params['cc_type']);
                        unset($params['cc_number']);
                        unset($params['cc_exp_month']);
                        unset($params['cc_exp_year']);
                        unset($params['cc_cid']);
                        $this->dataObjectHelper->populateWithArray($paymentData, $params, PaymentInterface::class);
                        $this->paymentRepository->save($paymentData);
                        if (isset($payment['api_status']) && $payment['api_status'] == 1) {
                            $this->_redirect('cc-authorization-success.html');
                        }
                    } else {
                        $this->messageManager->addError(__('Unable to process payment request.'));
                    }
                } else {
                    $this->messageManager->addError(__('Please enter all fields.'));
                }
            }
        } catch (\Exception $e) {
            $this->messageManager->addError(__('Unable to process payment request'));
            $this->logLoggerInterface->error($e->getMessage());
        }
        $this->_redirect($url);
    }

    /**
     * sending email to user
     * @param $params
     * @param $msg
     * @return bool
     */
    public function sendEmail($params, $msg)
    {

        $arrayVar = ['invoice_number' => $params['invoice_number'],
            'cc_amount' => '$' . number_format($params['cc_amount'], 2),
            'cc_owner' => $params['cc_owner'],
            'billing_street_address' => $params['billing_street_address'],
            'billing_city' => $params['billing_city'],
            'billing_state' => $params['billing_state'],
            'paymentstatus' => $msg,
            'card_type' => $this->helperData->getCardType($params['cc_type']),
            'remote_ip' => $this->remoteAddress->getRemoteAddress()
        ];
        $this->inlineTranslation->suspend();
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $sentToEmail = $this->scopeConfig->getValue('trans_email/ident_general/email', self::EMAIL);
        $sentToName = $this->scopeConfig->getValue('trans_email/ident_general/name', self::EMAILNAME);
        $sender = [
            'name' => $sentToName,
            'email' => $sentToEmail
        ];
        /**
         * geting emails;
         */
        $emailsArray = $this->helperData->quotePaymentEmails($params['short_name']);
        $ccEmail = $emailsArray['email_cc'];

        if (empty($ccEmail)) {
            $ccEmail = $emailsArray['add_cc_email'];
        }
        try {
            $transport = $this->transportBuilder
                ->setTemplateIdentifier(5)
                ->setTemplateOptions(
                    [
                        'area' => 'frontend',
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars($arrayVar)
                ->setFrom($sender)
                ->addTo($emailsArray['email'], $sentToName)
                ->addCc($ccEmail, $sentToName)
                ->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->logLoggerInterface->error($e->getMessage());
        }
    }
}
