<?php
/**
 * Techies India Inc. 2020
 */

namespace Techies\RequestQuotePayment\Controller\Adminhtml\Payment;

use Techies\RequestQuotePayment\Api\PaymentRepositoryInterface;
use Techies\RequestQuotePayment\Api\Data\PaymentInterface;
use Techies\RequestQuotePayment\Api\Data\PaymentInterfaceFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Registry;

/**
 * Class Save
 * this class use for save for color
 * this is action class
 */
class Save extends Action
{
    /**
     * Payment authorization factory
     * @var PaymentInterfaceFactory
     */
    protected $paymentFactory;
    /**
     * Data Object Processor
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;
    /**
     * Data Object Helper
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;
    /**
     * Data Persistor
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    /**
     * Core registry
     * @var Registry
     */
    protected $registry;
    /**
     * Payment authorization repository
     * @var PaymentRepositoryInterface
     */
    protected $paymentRepository;

    /**
     * Save constructor.
     * @param Context $context
     * @param PaymentInterfaceFactory $paymentFactory
     * @param PaymentRepositoryInterface $paymentRepository
     * @param DataObjectProcessor $dataObjectProcessor
     * @param DataObjectHelper $dataObjectHelper
     * @param DataPersistorInterface $dataPersistor
     * @param Registry $registry
     */
    public function __construct(
        Context $context,
        PaymentInterfaceFactory $paymentFactory,
        PaymentRepositoryInterface $paymentRepository,
        DataObjectProcessor $dataObjectProcessor,
        DataObjectHelper $dataObjectHelper,
        DataPersistorInterface $dataPersistor,
        Registry $registry
    ) {
        $this->paymentFactory = $paymentFactory;
        $this->paymentRepository = $paymentRepository;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataPersistor = $dataPersistor;
        $this->registry = $registry;
        parent::__construct($context);
    }

    /**
     * run the action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var PaymentInterface $payment */
        $payment = null;
        $postData = $this->getRequest()->getPostValue();
        $data = $postData;
        $id = !empty($data['payment_id']) ? $data['payment_id'] : null;
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            if ($id) {
                $payment = $this->paymentRepository->get((int)$id);
            } else {
                unset($data['payment_id']);
                $payment = $this->paymentFactory->create();
            }
            $this->dataObjectHelper->populateWithArray($payment, $data, PaymentInterface::class);
            $this->paymentRepository->save($payment);
            $this->messageManager->addSuccessMessage(__('You saved the payment authorization'));
            $this->dataPersistor->clear('techies_request_quote_payment_payment');
            if ($this->getRequest()->getParam('back')) {
                $resultRedirect->setPath('*/*/edit', ['payment_id' => $payment->getId()]);
            } else {
                $resultRedirect->setPath('*/*');
            }
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->dataPersistor->set('techies_request_quote_payment_payment', $postData);
            $resultRedirect->setPath('*/*/edit', ['payment_id' => $id]);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was a problem saving the payment authorization'));
            $this->dataPersistor->set('techies\request_quote_payment_payment', $postData);
            $resultRedirect->setPath('*/*/edit', ['payment_id' => $id]);
        }
        return $resultRedirect;
    }
}
