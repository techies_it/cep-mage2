<?php
/**
 * Techies India Inc. 2020
 */

namespace Techies\RequestQuotePayment\Controller\Adminhtml\Payment;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Techies\RequestQuotePayment\Api\PaymentRepositoryInterface;

class Edit extends Action
{
    /**
     * @var PaymentRepositoryInterface
     */
    private $paymentRepository;
    /**
     * @var Registry
     */
    private $registry;

    /**
     * Edit constructor.
     * @param Context $context
     * @param PaymentRepositoryInterface $paymentRepository
     * @param Registry $registry
     */
    public function __construct(
        Context $context,
        PaymentRepositoryInterface $paymentRepository,
        Registry $registry
    ) {
        $this->paymentRepository = $paymentRepository;
        $this->registry = $registry;
        parent::__construct($context);
    }

    /**
     * get current payment authorization
     *
     * @return null|\Techies\RequestQuotePayment\Api\Data\PaymentInterface
     */
    private function initPayment()
    {
        $paymentId = $this->getRequest()->getParam('payment_id');
        try {
            $payment = $this->paymentRepository->get($paymentId);
        } catch (NoSuchEntityException $e) {
            $payment = null;
        }
        $this->registry->register('current_payment', $payment);
        return $payment;
    }

    /**
     * Edit or create payment authorization
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $payment = $this->initPayment();
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu('Techies_RequestQuotePayment::requestquotepayment_payment');
        $resultPage->getConfig()->getTitle()->prepend(__('Payment Authorizations'));

        if ($payment === null) {
            $resultPage->getConfig()->getTitle()->prepend(__('New payment authorization'));
        } else {
            $resultPage->getConfig()->getTitle()->prepend($payment->getCcOwner());
        }
        return $resultPage;
    }
}
