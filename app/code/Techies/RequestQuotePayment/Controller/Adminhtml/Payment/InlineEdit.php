<?php
/**
 * Techies India Inc. 2020
 */

namespace Techies\RequestQuotePayment\Controller\Adminhtml\Payment;

use Techies\RequestQuotePayment\Api\PaymentRepositoryInterface;
use Techies\RequestQuotePayment\Api\Data\PaymentInterface;
use Techies\RequestQuotePayment\Model\ResourceModel\Payment as PaymentResourceModel;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Reflection\DataObjectProcessor;

/**
 * Class InlineEdit
 * this class use for edit data on grid and save
 */
class InlineEdit extends Action
{
    /**
     * Payment authorization repository
     * @var PaymentRepositoryInterface
     */
    protected $paymentRepository;
    /**
     * Data object processor
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;
    /**
     * Data object helper
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;
    /**
     * JSON Factory
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * Payment authorization resource model
     * @var PaymentResourceModel
     */
    protected $paymentResourceModel;

    /**
     * constructor
     * @param Context $context
     * @param PaymentRepositoryInterface $paymentRepository
     * @param DataObjectProcessor $dataObjectProcessor
     * @param DataObjectHelper $dataObjectHelper
     * @param JsonFactory $jsonFactory
     * @param PaymentResourceModel $paymentResourceModel
     */
    public function __construct(
        Context $context,
        PaymentRepositoryInterface $paymentRepository,
        DataObjectProcessor $dataObjectProcessor,
        DataObjectHelper $dataObjectHelper,
        JsonFactory $jsonFactory,
        PaymentResourceModel $paymentResourceModel
    ) {
        $this->paymentRepository = $paymentRepository;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->jsonFactory = $jsonFactory;
        $this->paymentResourceModel = $paymentResourceModel;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData([
                'messages' => [__('Please correct the data sent.')],
                'error' => true,
            ]);
        }

        foreach (array_keys($postItems) as $paymentId) {
            /** @var \Techies\RequestQuotePayment\Model\Payment|
             * \Techies\RequestQuotePayment\Api\Data\PaymentInterface $payment
             */
            try {
                $payment = $this->paymentRepository->get((int)$paymentId);
                $paymentData = $postItems[$paymentId];
                $this->dataObjectHelper->populateWithArray($payment, $paymentData, PaymentInterface::class);
                $this->paymentResourceModel->saveAttribute($payment, array_keys($paymentData));
            } catch (LocalizedException $e) {
                $messages[] = $this->getErrorWithPaymentId($payment, $e->getMessage());
                $error = true;
            } catch (\RuntimeException $e) {
                $messages[] = $this->getErrorWithPaymentId($payment, $e->getMessage());
                $error = true;
            } catch (\Exception $e) {
                $messages[] = $this->getErrorWithPaymentId(
                    $payment,
                    __('Something went wrong while saving the Payment authorization.')
                );
                $error = true;
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * Add Payment authorization id to error message
     *
     * @param \Techies\RequestQuotePayment\Api\Data\PaymentInterface $payment
     * @param string $errorText
     * @return string
     */
    protected function getErrorWithPaymentId(PaymentInterface $payment, $errorText)
    {
        return '[Payment authorization ID: ' . $payment->getId() . '] ' . $errorText;
    }
}
