<?php
/**
 * Techies India Inc. 2020
 */

namespace Techies\RequestQuotePayment\Test\Unit\Block\Adminhtml\Button\Payment;

use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use PHPUnit\Framework\TestCase;
use Techies\RequestQuotePayment\Api\Data\PaymentInterface;
use Techies\RequestQuotePayment\Block\Adminhtml\Button\Payment\Delete;

class DeleteTest extends TestCase
{
    /**
     * @var UrlInterface | \PHPUnit_Framework_MockObject_MockObject
     */
    private $url;
    /**
     * @var Registry | \PHPUnit_Framework_MockObject_MockObject
     */
    private $registry;
    /**
     * @var Delete
     */
    private $button;

    /**
     * set up tests
     */
    protected function setUp()
    {
        $this->url = $this->createMock(UrlInterface::class);
        $this->registry = $this->createMock(Registry::class);
        $this->button = new Delete($this->registry, $this->url);
    }

    /**
     * @covers \Techies\RequestQuotePayment\Block\Adminhtml\Button\Payment\Delete::getButtonData()
     */
    public function testButtonDataNoPayment()
    {
        $this->registry->method('registry')->willReturn(null);
        $this->url->expects($this->exactly(0))->method('getUrl');
        $this->assertEquals([], $this->button->getButtonData());
    }

    /**
     * @covers \Techies\RequestQuotePayment\Block\Adminhtml\Button\Payment\Delete::getButtonData()
     */
    public function testButtonDataNoPaymentId()
    {
        $payment = $this->createMock(PaymentInterface::class);
        $payment->method('getId')->willReturn(null);
        $this->registry->method('registry')->willReturn($payment);
        $this->url->expects($this->exactly(0))->method('getUrl');
        $this->assertEquals([], $this->button->getButtonData());
    }

    /**
     * @covers \Techies\RequestQuotePayment\Block\Adminhtml\Button\Payment\Delete::getButtonData()
     */
    public function testButtonData()
    {
        $payment = $this->createMock(PaymentInterface::class);
        $payment->method('getId')->willReturn(2);
        $this->registry->method('registry')->willReturn($payment);
        $this->url->expects($this->once())->method('getUrl');
        $data = $this->button->getButtonData();
        $this->assertArrayHasKey('on_click', $data);
        $this->assertArrayHasKey('label', $data);
        $this->assertArrayHasKey('class', $data);
    }
}
