<?php
/**
 * Techies India Inc. 2020
 */

namespace Techies\RequestQuotePayment\Test\Unit\Model\Payment\Executor;

use PHPUnit\Framework\TestCase;
use Techies\RequestQuotePayment\Api\PaymentRepositoryInterface;
use Techies\RequestQuotePayment\Api\Data\PaymentInterface;
use Techies\RequestQuotePayment\Model\Payment\Executor\Delete;

class DeleteTest extends TestCase
{
    /**
     * @covers \Techies\RequestQuotePayment\Model\Payment\Executor\Delete::execute()
     */
    public function testExecute()
    {
        /** @var PaymentRepositoryInterface | \PHPUnit_Framework_MockObject_MockObject $paymentRepository */
        $paymentRepository = $this->createMock(PaymentRepositoryInterface::class);
        $paymentRepository->expects($this->once())->method('deleteById');
        /** @var PaymentInterface | \PHPUnit_Framework_MockObject_MockObject $payment */
        $payment = $this->createMock(PaymentInterface::class);
        $delete = new Delete($paymentRepository);
        $delete->execute($payment->getId());
    }
}
