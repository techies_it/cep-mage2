<?php
/*
 * Techies India Inc.
 */

namespace Techies\Upsrate\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Json\Helper\Data as JsonHelperData ;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    private $jsonHelper;

    public function __construct(ScopeConfigInterface $scopeConfig, JsonHelperData $jsonHelper)
    {
        $this->scopeConfig = $scopeConfig;
        $this->jsonHelper = $jsonHelper;
    }

    /**
     * @return boolean, checking isEnabled function
     */
    public function isEnabled()
    {
        return (bool)$this->scopeConfig->getValue(
            'techies_upsrate/upsrate/upsrate_active',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
