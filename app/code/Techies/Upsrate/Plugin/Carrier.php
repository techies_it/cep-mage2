<?php
/*
 * Techies India Inc.
 */

namespace Techies\Upsrate\Plugin;

use Techies\Flatrate\Model\Flatrate;
use Techies\Flatrate\Helper\Data;
use Psr\Log\LoggerInterface;
use Techies\Upsrate\Helper\Data as UpsrateHelperData;
use \Magento\Ups\Model\Carrier as SubjectCarrier;
use Magento\Shipping\Model\Rate\Result;
use Magento\Quote\Model\Quote\Address\RateRequest;

/**
 * UPS shipping implementation for ground method custom price
 */
class Carrier
{
    /**
     * @var \Techies\Flatrate\Helper\Data
     */
    protected $helper;
    /**
     * @var \Techies\Flatrate\Model\Flatrate
     */
    protected $rate;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    protected $upshelper;

    /**
     * @var \Techies\Upsrate\Helper\Data
     */

    public function __construct(Flatrate $rate, Data $helper, LoggerInterface $logger, UpsrateHelperData $upshelper)
    {
        $this->helper = $helper;
        $this->logger = $logger;
        $this->rate = $rate;
        $this->upshelper = $upshelper;
    }

    // in-progress depend on cart functionality
    public function afterCollectRates(SubjectCarrier $subject, Result $results, RateRequest $rateRequest)
    {
        try {

            /*
            * Isenbale ups custom funtionality
            */
            $isEnable = $this->upshelper->isEnabled();
            $qty = $rateRequest->getPackageQty();
            /*
             * get sample max qty
            */
            $maxQty = $this->helper->sampleProductMaxQty();
            $shippinPrice = $this->helper->flatrateRangesPriceByQty($qty); // geting price by qty range
            if ($isEnable && $qty <= $maxQty && empty($shippinPrice)) {
                $shippingRates = $results->getAllRates();
                $price = $this->rate->getRequestSampleProductPrice(); // geting sample product price
                if (isset($price) && !empty($price)) {
                    foreach ($shippingRates as $rate) { // set custom rate for ground shipping method 03 ups ground
                        if ($rate->getMethod() == 03) {
                            $rate->setCost($price);
                            $rate->setPrice($price);
                        }
                    }
                }
            }

        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }

        return $results;
    }
}
