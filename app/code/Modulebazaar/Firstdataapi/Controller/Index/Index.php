<?php
/* Copyright (C) 2012-2018 eGrove Systems Corporation - All Rights Reserved
* Proprietary and confidential
* Unauthorized copying of this file, via any medium is strictly prohibited
* This file is part of Modulebazaar_Firstdatapi.
* Modulebazaar_Firstdatapi can not be copied and/or distributed without the express
* permission of eGrove Systems Corporation
*/

namespace Modulebazaar\Firstdataapi\Controller\Index;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Controller\ResultFactory;
use Magento\Sales\Model\Order\Payment\Transaction;

/**
 * Class Index
 *
 * @package Modulebazaar\Firstdataapi\Controller\Index
 */
class Index extends \Magento\Framework\App\Action\Action
{
    const FIRST_DATA_DEMO_ENDPOINT = 'https://api.demo.globalgatewaye4.firstdata.com/transaction/';
    const FIRST_DATA_ENDPOINT = 'https://api.globalgatewaye4.firstdata.com/transaction/';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Escaper
     */
    protected $escaper;

    /**
     * Index constructor.
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Sales\Model\Order\Email\Sender\OrderSender $ordersender
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Sales\Model\Service\InvoiceService $invoiceService
     * @param \Magento\Framework\DB\Transaction $transaction
     * @param CheckoutSession $checkoutSession
     * @param \Magento\Sales\Model\OrderNotifier $notifier
     * @param \Magento\Framework\HTTP\Client\Curl $curl
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     * @param \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoiceSender
     * @param \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface $transactionBuilder
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $ordersender,
        \Magento\Framework\Escaper $escaper,
        \Magento\Sales\Model\Service\InvoiceService $invoiceService,
        \Magento\Framework\DB\Transaction $transaction,
        CheckoutSession $checkoutSession,
        \Magento\Sales\Model\OrderNotifier $notifier,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoiceSender,
        \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface $transactionBuilder
    ) {
        parent::__construct($context);
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->escaper = $escaper;
        $this->curl = $curl;
        $this->ordersender = $ordersender;
        $this->notifier = $notifier;
        $this->quoteFactory = $quoteFactory;
        $this->checkoutSession = $checkoutSession;
        $this->orderFactory = $orderFactory;
        $this->_invoiceService = $invoiceService;
        $this->_transaction = $transaction;
        $this->_transactionBuilder = $transactionBuilder;
        $this->invoiceSender = $invoiceSender;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $session = $this->checkoutSession;
        $quote = $this->checkoutSession->getLastRealOrder();
        $order = $this->orderFactory->create()->loadByIncrementId($quote->getData('increment_id'));
        $billingaddress = $order->getBillingAddress();
        $currencyDesc = $order->getOrderCurrencyCode();
        $totals = $order->getGrandTotal();
        $month = $order->getPayment()->getCcExpMonth();
        $year = substr($order->getPayment()->getCcExpYear(), 2, 2);
        if (strlen($month) == 1) {
            $expyear = "0" . $month . $year;
        } else {
            $expyear = $month . "" . $year;
        }

        $data = [
            'vpc_Version' => 1,
            'vpc_Command' => 'firstdata',
            'vpc_Merchant' => trim($this->scopeConfig->getValue('payment/firstdataapi/gateway_id', 'stores')),
            'vpc_AccessCode' => trim($this->scopeConfig->getValue('payment/firstdataapi/gateway_pwd', 'stores')),
            'vpc_Amount' => $totals,
            'vpc_CardName' => 'firstdataapi',
            'vpc_Cardtypename' => $order->getPayment()->getData('cc_type'),
            'vpc_CardNum' => $session->getCcnumber(),
            'vpc_CardExp' => $expyear,
            'vpc_Message' => 'transaction',
            'vpc_MerchTxnRef' => 'test',
            'billing_cust_name' => $order->getCustomerFirstname(),
            'billing_last_name' => $order->getCustomerLastname(),
            'billing_cust_tel_No' => $billingaddress->getTelephone(),
            'billing_cust_email' => $order->getCustomerEmail(),
            'billing_cust_address' => '',
            'billing_cust_city' => $billingaddress->getCity(),
            'billing_cust_country' => $billingaddress->getCountryId(),
            'billing_cust_state' => $billingaddress->getRegion(),
            'billing_cust_zip' => $billingaddress->getPostcode(),
            'Order_Id' => $order->getIncrementId()
        ];

        $cvv = $session->getCcid();
        if ($cvv) {
            $data['vpc_CardSecurityCode'] = $cvv;
        }
        $data['vpc_Currency'] = $currencyDesc;
        $username = trim($this->scopeConfig->getValue('payment/firstdataapi/username', 'stores'));
        $gatewayId = trim($this->scopeConfig->getValue('payment/firstdataapi/gateway_id', 'stores'));
        $hmackey = trim($this->scopeConfig->getValue('payment/firstdataapi/hmac_key', 'stores'));
        $gatewayPassword = trim($this->scopeConfig->getValue('payment/firstdataapi/gateway_pwd', 'stores'));
        $transaction_mode = trim($this->scopeConfig->getValue('payment/firstdataapi/transaction_mode', 'stores'));
        $tran_type = trim($this->scopeConfig->getValue('payment/firstdataapi/transaction_type', 'stores'));
        $api_version = trim($this->scopeConfig->getValue('payment/firstdataapi/api_version', 'stores'));
        $amount = str_replace(",", "", number_format($totals, 2));
        $expDate = $data['vpc_CardExp'];
        $trxnProperties = [
            "User_Name" => $username,
            "gateway_id" => $gatewayId,
            "password" => $gatewayPassword,
            "Secure_AuthResult" => "",
            "ecommerce_flag" => "0",
            "xid" => "",
            "cavv" => "",
            "cavv_algorithm" => "",
            "transaction_type" => $tran_type,
            "reference_no" => $data["Order_Id"],
            "customer_ref" => "",
            "client_ip" => $_SERVER["REMOTE_ADDR"],
            "client_email" => $data["billing_cust_email"],
            "language" => "en",
            "cc_number" => $data["vpc_CardNum"],
            "Card_Type" => $data["vpc_Cardtypename"],
            "cc_expiry" => $expDate,
            "cardholder_name" => $data["vpc_CardName"],
            "track1" => "",
            "track2" => "",
            "transaction_tag" => "",
            "amount" => $amount,
            "cc_verification_str1" => substr($data["billing_cust_address"], 0, 28) . "|" . $data["billing_cust_zip"],
            "cc_verification_str2" => $cvv,
            "cvd_presence_ind" => "",
            "secure_auth_required" => "",
            "currency_code" => $currencyDesc,
            "partial_redemption" => "",
            "zipCode" => $data["billing_cust_zip"],
            "tax1_amount" => "",
            "tax1_number" => "",
            "tax2_amount" => "",
            "tax2_number" => "",
            "surchargeamount" => "",
            "pan" => ""
        ];
        /**
         * First data Endpoint based on mode
         */
        if ($transaction_mode == 'test') {
            $post_url = self::FIRST_DATA_DEMO_ENDPOINT . $api_version;
        } else {
            $post_url = self::FIRST_DATA_ENDPOINT . $api_version;
        }
        //HMAC Calculation
        $data_string = json_encode($trxnProperties);
        $content_digest = sha1($data_string);
        $current_time = gmdate('Y-m-dTH:i:s') . 'Z';
        $current_time = str_replace('GMT', 'T', $current_time);
        $code_string = "POST\napplication/json\n{$content_digest}\n{$current_time}\n/transaction/" . $api_version;
        $code = base64_encode(hash_hmac('sha1', $code_string, $hmackey, true));
        $authorizationValue = 'GGE4_API ' . $gatewayId . ':' . $code;
        //Using Curl to Make Payment Using First data
        $this->curl->addHeader('Content-Type', 'application/json');
        $this->curl->addHeader('Content-Length', strlen($data_string));
        $this->curl->addHeader('X-GGe4-Content-SHA1', $content_digest);
        $this->curl->addHeader('X-GGe4-Date', $current_time);
        $this->curl->addHeader('Authorization', 'GGE4_API ' . $gatewayId . ':' . $code);
        $this->curl->post(
            $post_url,
            $data_string
        );
        $result = $this->curl->getBody();
        //End Curl
        //Check the Transaction Result
        if ($result == "Unauthorized Request. Bad or missing credentials.") {
            return $this->paymentCredentialFailure($order, $result);
        }
        $data_string = json_decode($result);
        if (json_last_error() == JSON_ERROR_NONE) {
        } else {
            return $this->paymentCredentialFailure($order, $result);
        }
        unset($data_string->cc_expiry);
        unset($data_string->cc_verification_str2);
        if ($data_string->transaction_approved == 1) {
            $order = $this->orderFactory->create()->loadByIncrementId($quote->getData('increment_id'));
            $orderEmail = $this->ordersender;
            $orderEmail->sendConfirmationFinal($order);
            $order->setState(trim($this->scopeConfig->getValue('payment/firstdataapi/order_status', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)));
            $order->setStatus(trim($this->scopeConfig->getValue('payment/firstdataapi/order_status', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)));
            $order->setCanSendNewEmailFlag(1);
            $order->setEmailSent(true);

            //Create invoice If authorize and capture
            if ($tran_type == '00') {
                $this->createInvoiceApprovedOrder($order);
            }
            $this->createFirstdataTransaction($data_string, $tran_type, $order);

            $this->notifier->notify($order);
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('checkout/onepage/success');
            return $resultRedirect;
        } else {
            $msg = $result;
            if ($order->getId()) {
                if ($order->getState() != 'canceled') {
                    $order->registerCancellation($msg)->save();
                }
                $quotes = $this->quoteFactory->create()->load($order->getQuoteId());
                if ($quotes->getId()) {
                    $quotes->setIsActive(1)->setReservedOrderId(null)->save();
                    $sessions = $this->checkoutSession;
                    $sessions->replaceQuote($quotes);
                }
                $sessions->unsLastRealOrderId();
                $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                $resultRedirect->setPath('checkout/cart/index');
                $this->messageManager->addError($msg);

                return $resultRedirect;
            }
        }
    }

    /**
     * paymentCrendentialFailure
     *
     * @param $order
     * @param $paymentresult
     * @return \Magento\Framework\Controller\ResultInterface
     */
    protected function paymentCredentialFailure($order, $paymentresult)
    {
        $msge = $paymentresult;
        if ($order->getId()) {
            if ($order->getState() != 'canceled') {
                $order->registerCancellation($msge)->save();
            }
            $quotes = $this->quoteFactory->create()->load($order->getQuoteId());
            if ($quotes->getId()) {
                $quotes->setIsActive(1)->setReservedOrderId(null)->save();
                $sessions = $this->checkoutSession;
                $sessions->replaceQuote($quotes);
            }
            $sessions->unsLastRealOrderId();
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('checkout/cart/index');
            $this->messageManager->addError($msge);
            return $resultRedirect;
        }
    }

    /**
     * createInvoiceApprovedOrder
     *
     * @param $order
     * @return \Magento\Framework\Controller\ResultInterface
     */
    protected function createInvoiceApprovedOrder($order)
    {
        try {
            if ($order->canInvoice()) {
                $invoice = $this->_invoiceService->prepareInvoice($order);
                $invoice->register();
                $invoice->save();
                $transactionSave = $this->_transaction->addObject(
                    $invoice
                )->addObject(
                    $invoice->getOrder()
                );
                $transactionSave->save();
                $this->invoiceSender->send($invoice);
                $order->setState(trim($this->scopeConfig->getValue('payment/firstdataapi/order_status', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)));
                $order->setStatus(trim($this->scopeConfig->getValue('payment/firstdataapi/order_status', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)));

                //send notification code
                $order->addStatusHistoryComment(
                    __('Notified customer about invoice #%1.', $invoice->getId())
                )
                    ->setIsCustomerNotified(true)
                    ->save();
            }
        } catch (\Exception $e) {
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('checkout/cart/index');
            $this->messageManager->addError($e->getMessage());
            return $resultRedirect;
        }
    }

    /**
     * createFirstdataTransaction
     *
     * @param $paymentData
     * @param $tran_type
     * @param null $order
     * @return \Magento\Framework\Controller\ResultInterface
     */
    protected function createFirstdataTransaction($paymentData, $tran_type, $order = null)
    {
        try {
            $payment = $order->getPayment();
            $payment->setLastTransId($paymentData->transaction_tag);
            $payment->setTransactionId($paymentData->transaction_tag);
            $payment->setAdditionalInformation(
                [Transaction::RAW_DETAILS => (array)$paymentData]
            );
            $formatedPrice = $order->getBaseCurrency()->formatTxt(
                $order->getGrandTotal()
            );
            $message = __('The  amount is %1.', $formatedPrice);
            if ($tran_type == '00') {
                $tranTypeValues = Transaction::TYPE_CAPTURE;
            } else {
                $tranTypeValues = Transaction::TYPE_AUTH;
            }
            $transaction = $this->_transactionBuilder->setPayment($payment)
                ->setOrder($order)
                ->setTransactionId($paymentData->transaction_tag)
                ->setAdditionalInformation(
                    [Transaction::RAW_DETAILS => (array)$paymentData]
                )
                ->setFailSafe(true)
                ->build($tranTypeValues);
            $payment->addTransactionCommentsToOrder(
                $transaction,
                $message
            );
            $payment->setParentTransactionId(null);
            $payment->save();
            return $transaction->save()->getTransactionId();
        } catch (\Exception $e) {
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('checkout/cart/index');
            $this->messageManager->addError($e->getMessage());
            return $resultRedirect;
        }
    }
}
