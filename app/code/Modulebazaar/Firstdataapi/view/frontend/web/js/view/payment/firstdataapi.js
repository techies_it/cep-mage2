/**
 * Copyright (C) 2012-2018 eGrove Systems Corporation - All Rights Reserved
 * Proprietary and confidential
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * This file is part of Modulebazaar_Firstdatapi.
 * Modulebazaar_Firstdatapi can not be copied and/or distributed without the express
 * permission of eGrove Systems Corporation
 */
/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component,rendererList) {
        'use strict';
        rendererList.push(
            {
                type: 'firstdataapi',
                component: 'Modulebazaar_Firstdataapi/js/view/payment/method-renderer/firstdataapi-method'
            }
        );
        return Component.extend({});
    }
);
