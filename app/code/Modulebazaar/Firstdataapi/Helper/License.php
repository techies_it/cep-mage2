<?php
/* Copyright (C) 2012-2018 eGrove Systems Corporation - All Rights Reserved
* Proprietary and confidential
* Unauthorized copying of this file, via any medium is strictly prohibited
* This file is part of Modulebazaar_Firstdatapi.
* Modulebazaar_Firstdatapi can not be copied and/or distributed without the express
* permission of eGrove Systems Corporation
*/
namespace Modulebazaar\Firstdataapi\Helper;

class License extends \Magento\Framework\App\Helper\AbstractHelper
{

    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    public function getValue()
    {
        $types = $this->scopeConfig->getValue(
            'payment/firstdataapi/transaction_type',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return $types;
    }
}
