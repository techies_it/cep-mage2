<?php
/* Copyright (C) 2012-2018 eGrove Systems Corporation - All Rights Reserved
* Proprietary and confidential
* Unauthorized copying of this file, via any medium is strictly prohibited
* This file is part of Modulebazaar_Firstdatapi.
* Modulebazaar_Firstdatapi can not be copied and/or distributed without the express
* permission of eGrove Systems Corporation
*/
namespace Modulebazaar\Firstdataapi\Model\Config\Source\Order\Status;

use Magento\Sales\Model\Order;
use Magento\Sales\Model\Config\Source\Order\Status;

/**
 * Order Status source model
 */
class Pendingpayment extends Status
{
    /**
     * @var string[]
     */
    protected $_stateStatuses = [Order::STATE_PENDING_PAYMENT];
}
