<?php
/* Copyright (C) 2012-2018 eGrove Systems Corporation - All Rights Reserved
* Proprietary and confidential
* Unauthorized copying of this file, via any medium is strictly prohibited
* This file is part of Modulebazaar_Firstdatapi.
* Modulebazaar_Firstdatapi can not be copied and/or distributed without the express
* permission of eGrove Systems Corporation
*/
namespace Modulebazaar\Firstdataapi\Model\Source;

class Type
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return
            array(
                array("value" => '01', "label" => 'Authorize Only'),
                array("value" => '00', "label" => 'Authorize and Capture'),
            );
    }
}
