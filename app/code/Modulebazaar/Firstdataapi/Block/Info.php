<?php
/* Copyright (C) 2012-2018 eGrove Systems Corporation - All Rights Reserved
* Proprietary and confidential
* Unauthorized copying of this file, via any medium is strictly prohibited
* This file is part of Modulebazaar_Firstdatapi.
* Modulebazaar_Firstdatapi can not be copied and/or distributed without the express
* permission of eGrove Systems Corporation
*/
namespace Modulebazaar\Firstdataapi\Block;

class Info extends \Magento\Payment\Block\Info\Cc
{
    /**
     * @var bool
     */
    protected $isEcheck = false;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Payment\Model\Config $paymentConfig
     * @param \Modulebazaar\Firstdataapi\Helper\License $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Payment\Model\Config $paymentConfig,
        \Modulebazaar\Firstdataapi\Helper\License $helper,
        array $data = []
    ) {
        $this->helper = $helper;
        parent::__construct($context, $paymentConfig, $data);
    }

    /**
     * Prepare credit card related payment info
     *
     * @param \Magento\Framework\DataObject|array $transport
     * @return \Magento\Framework\DataObject
     */
    protected function _prepareSpecificInformation($transport = null)
    {
        if (null !== $this->_paymentSpecificInformation) {
            return $this->_paymentSpecificInformation;
        }
        $transport = \Magento\Payment\Block\Info::_prepareSpecificInformation($transport);
        $data = [];

        /** @var \Magento\Sales\Model\Order\Payment $info */
        $info = $this->getInfo();

        $ccType = $this->getCcTypeName();
        if (!empty($ccType) && $ccType != 'N/A') {
            $data[(string)__('Credit Card Type')] = $ccType;
        }

        if ($info->getCcLast4()) {
            $data[(string)__('Credit Card Number')] = sprintf('XXXX-%s', $info->getCcLast4());
        }
        if ($info->getCcExpMonth()) {
            $data[(string)__('Expiry Month')] = $info->getCcExpMonth() . ' / ' . $info->getCcExpYear();
        }

        $types = $this->helper->getValue();

        if ($types == '00') {
            $type = "Authorize and Capture";
        } elseif ($types == '01') {
            $type = "Authorize Only";
        }
        if ($info->getCcOwner()) {
            $data[(string)__('Name')] = $info->getCcOwner();
        }

        $data[(string)__('Type')] = $type;
        $transport->setData(array_merge($data, $transport->getData()));
        return $transport;
    }
}
